(function ($) {
	$(window).load(function(){
		bindHeaderScroll();
	});

	$(document).ready(function(){
		//add target="_BLANK" to some elements
		$('ul.info-items-icon a, ul.social-icon a').attr('target', '_blank');
	});

	function bindHeaderScroll(){
		var $nav = $('nav.navbar');
		var scrolled = false;
		var offset = $('.wol-header-slider').outerHeight() || $nav.outerHeight();
		$(window).scroll(function () {
			if (offset < $(window).scrollTop() && !scrolled) {
				$nav.addClass('fixed');
				$nav.animate({ top: 0 }, 1000);
				scrolled = true;
			}
			
			if (offset > $(window).scrollTop() && scrolled) {
				scrolled = false;
				$nav.animate({ top: -80 }, 100, function () {
					$nav.removeClass('fixed');
					$nav.removeAttr('style');
				});
			}
		});
	}

})(jQuery);

//for browsers which doesn't support object-fit (you can use babel to transpile to ES5)

if ('objectFit' in document.documentElement.style === false) {
	document.addEventListener('DOMContentLoaded', () => {
		Array.prototype.forEach.call(document.querySelectorAll('img[data-object-fit]'), image => {
			(image.runtimeStyle || image.style).background = `url("${image.src}") no-repeat 50%/${image.currentStyle ? image.currentStyle['object-fit'] : image.getAttribute('data-object-fit')}`
			image.src = `data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='${image.width}' height='${image.height}'%3E%3C/svg%3E`
		})
	})
}