<?php
/**
 * attorg functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package attorg
 */

/**
 * Define Attorg Folder Path & Url Const
 * @since 1.0.0
 * */
define('ATTORG_THEME_ROOT',get_template_directory());
define('ATTORG_THEME_ROOT_URL',get_template_directory_uri());
define('ATTORG_INC',ATTORG_THEME_ROOT .'/inc');
define('ATTORG_THEME_SETTINGS',ATTORG_INC.'/theme-settings');
define('ATTORG_THEME_SETTINGS_IMAGES',ATTORG_THEME_ROOT_URL.'/inc/theme-settings/images');
define('ATTORG_TGMA',ATTORG_INC.'/plugins/tgma');
define('ATTORG_DYNAMIC_STYLESHEETS',ATTORG_INC.'/dynamic-stylesheets');
define('ATTORG_CSS',ATTORG_THEME_ROOT_URL.'/assets/css');
define('ATTORG_JS',ATTORG_THEME_ROOT_URL.'/assets/js');
define('ATTORG_ASSETS',ATTORG_THEME_ROOT_URL.'/assets');

/**
 * Theme Initial File
 * @since 1.0.0
 * */
if (file_exists(ATTORG_INC .'/class-attorg-init.php')){
	require_once ATTORG_INC .'/class-attorg-init.php';
}
/**
 * Codester Framework Functions
 * @since 1.0.0
 * */
if (file_exists(ATTORG_INC .'/csf-functions.php')){
	require_once ATTORG_INC .'/csf-functions.php';
}
/**
 * theme helpers functions
 * @since 1.0.0
 * */
if (file_exists(ATTORG_INC .'/class-attorg-helper-functions.php')){

	require_once ATTORG_INC .'/class-attorg-helper-functions.php';
	if (!function_exists('Attorg')){
		function Attorg(){
			return class_exists('Attorg_Helper_Functions') ? new Attorg_Helper_Functions() : false;
		}
	}

}


