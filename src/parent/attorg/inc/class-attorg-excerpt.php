<?php
if (!defined('ABSPATH')){
	exit(); //exit if access it directly
}
/*
* Theme Excerpt Class
* @since 1.0.0
* @source https://gist.github.com/bgallagh3r/8546465
*/
if (!class_exists('Attorg_excerpt')):
class Attorg_excerpt {

    public static $length = 55;
    public static $types = array(
      'short' => 25,
      'regular' => 55,
      'long' => 100,
      'promo'=>15
    );

    public static $more = true;

    /**
    * Sets the length for the excerpt,
    * then it adds the WP filter
    * And automatically calls the_excerpt();
    *
    * @param string $new_length
    * @return void
    * @author Baylor Rae'
    */
    public static function length($new_length = 55, $more = true) {
        Attorg_excerpt::$length = $new_length;
        Attorg_excerpt::$more = $more;

        add_filter( 'excerpt_more', 'Attorg_excerpt::auto_excerpt_more' );

        add_filter('excerpt_length', 'Attorg_excerpt::new_length');

        Attorg_excerpt::output();
    }

    public static function new_length() {
        if( isset(Attorg_excerpt::$types[Attorg_excerpt::$length]) )
            return Attorg_excerpt::$types[Attorg_excerpt::$length];
        else
            return Attorg_excerpt::$length;
    }

    public static function output() {
        the_excerpt();
    }

    public static function continue_reading_link() {

        return '<span class="readmore"><a href="'.get_permalink().'">'.esc_html__('Read More','attorg').'</a></span>';
    }

    public static function auto_excerpt_more( ) {
        if (Attorg_excerpt::$more) :
            return ' ';
        else :
            return ' ';
        endif;
    }

} //end class
endif;

if (!function_exists('attorg_excerpt')){

	function attorg_excerpt($length = 55, $more=true) {
		Attorg_excerpt::length($length, $more);
	}

}


?>