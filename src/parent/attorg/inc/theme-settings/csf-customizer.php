<?php

/*
 * Theme Customize Options
 * @package attorg
 * @since 1.0.0
 * */

if ( !defined('ABSPATH') ){
	exit(); // exit if access directly
}

if (class_exists('CSF') ){
	$prefix = 'attorg';

	CSF::createCustomizeOptions($prefix.'_customize_options');
	/*-------------------------------------
	** Theme Main panel
-------------------------------------*/
	CSF::createSection($prefix.'_customize_options',array(
		'title' => esc_html__('Attorg Options','attorg'),
		'id' => 'attorg_main_panel',
		'priority' => 11,
	));


	/*-------------------------------------
		** Theme Main Color
	-------------------------------------*/
	CSF::createSection($prefix.'_customize_options',array(
		'title' => esc_html__('01. Main Color','attorg'),
		'priority' => 10,
		'parent' => 'attorg_main_panel',
		'fields' => array(
			array(
				'id'    => 'main_color',
				'type'  => 'color',
				'title' => esc_html__('Theme Main Color','attorg'),
				'default' => '#d0bf90',
				'desc' => esc_html__('This is theme primary color, means it\'ll affect most of elements that have default color of our theme primary color','attorg')
			),
			array(
				'id'    => 'secondary_color',
				'type'  => 'color',
				'title' => esc_html__('Theme Secondary Color','attorg'),
				'default' => '#30373f',
				'desc' => esc_html__('This is theme secondary color, means it\'ll affect most of elements that have default color of our theme secondary color','attorg')
			),
			array(
				'id'    => 'heading_color',
				'type'  => 'color',
				'title' => esc_html__('Theme Heading Color','attorg'),
				'default' => '#272b2e',
				'desc' => esc_html__('This is theme heading color, means it\'ll affect all of heading tag like, h1,h2,h3,h4,h5,h6','attorg')
			),
			array(
				'id'    => 'paragraph_color',
				'type'  => 'color',
				'title' => esc_html__('Theme Paragraph Color','attorg'),
				'default' => '#878a95',
				'desc' => esc_html__('This is theme paragraph color, means it\'ll affect all of body/paragraph tag like, p,li,a etc','attorg')
			),
		)
	));
	/*-------------------------------------
		** Theme Header Options
	-------------------------------------*/

	CSF::createSection( $prefix.'_customize_options', array(
		'title'  => esc_html__('02. Header One Options','attorg'),
		'parent' => 'attorg_main_panel',
		'priority' => 11,
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Nav Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_01_nav_bar_bg_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Background Color','attorg'),
				'default' => '#30373f'
			),
			array(
				'id' => 'header_01_nav_bar_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Text Color','attorg'),
				'default' => 'rgba(255, 255, 255, 0.8)'
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Dropdown Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_01_dropdown_bg_color',
				'type' => 'color',
				'title' => esc_html__('Dropdown Background Color','attorg'),
				'default' => '#ffffff'
			),
			array(
				'id' => 'header_01_dropdown_color',
				'type' => 'color',
				'title' => esc_html__('Dropdown Text Color','attorg'),
				'default' => '#878a95'
			)
		)
	));
	CSF::createSection( $prefix.'_customize_options', array(
		'title'  => esc_html__('03. Header Two Options','attorg'),
		'parent' => 'attorg_main_panel',
		'priority' => 11,
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Info Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_02_info_bar_bg_color',
				'type' => 'color',
				'title' => esc_html__('Info Bar Background Color','attorg'),
				'default' => '#F7F7F7'
			),
			array(
				'id' => 'header_02_info_bar_text_color',
				'type' => 'color',
				'title' => esc_html__('Info Bar Text Color','attorg'),
				'default' => '#878a95'
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Nav Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_02_nav_bar_bg_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Background Color','attorg'),
				'default' => '#30373f'
			),
			array(
				'id' => 'header_02_nav_bar_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Text Color','attorg'),
				'default' => 'rgba(255, 255, 255, 0.8)'
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Dropdown Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_02_dropdown_bg_color',
				'type' => 'color',
				'title' => esc_html__('Dropdown Background Color','attorg'),
				'default' => '#ffffff'
			),
			array(
				'id' => 'header_02_dropdown_color',
				'type' => 'color',
				'title' => esc_html__('Dropdown Text Color','attorg'),
				'default' => '#878a95'
			)
		)
	));
	CSF::createSection( $prefix.'_customize_options', array(
		'title'  => esc_html__('04. Header Three Options','attorg'),
		'parent' => 'attorg_main_panel',
		'priority' => 11,
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Nav Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_03_nav_bar_bg_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Background Color','attorg'),
				'default' => 'transparent'
			),
			array(
				'id' => 'header_03_nav_bar_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Text Color','attorg'),
				'default' => 'rgba(255, 255, 255, 0.8)'
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Dropdown Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_03_dropdown_bg_color',
				'type' => 'color',
				'title' => esc_html__('Dropdown Background Color','attorg'),
				'default' => '#ffffff'
			),
			array(
				'id' => 'header_03_dropdown_color',
				'type' => 'color',
				'title' => esc_html__('Dropdown Text Color','attorg'),
				'default' => '#878a95'
			)
		)
	));
	CSF::createSection( $prefix.'_customize_options', array(
		'title'  => esc_html__('05. Header Four Options','attorg'),
		'parent' => 'attorg_main_panel',
		'priority' => 11,
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Info Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_04_info_bar_bg_color',
				'type' => 'color',
				'title' => esc_html__('Info Bar Background Color','attorg'),
				'default' => '#30373f'
			),
			array(
				'id' => 'header_04_info_bar_text_color',
				'type' => 'color',
				'title' => esc_html__('Info Bar Text Color','attorg'),
				'default' => 'rgba(255, 255, 255, 0.8)'
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Nav Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_04_nav_bar_bg_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Background Color','attorg'),
				'default' => 'transparent'
			),
			array(
				'id' => 'header_04_nav_bar_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Text Color','attorg'),
				'default' => 'rgba(255, 255, 255, 0.8)'
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Dropdown Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_04_dropdown_bg_color',
				'type' => 'color',
				'title' => esc_html__('Dropdown Background Color','attorg'),
				'default' => '#ffffff'
			),
			array(
				'id' => 'header_04_dropdown_color',
				'type' => 'color',
				'title' => esc_html__('Dropdown Text Color','attorg'),
				'default' => '#878a95'
			)
		)
	));
	/* header five */
	CSF::createSection( $prefix.'_customize_options', array(
		'title'  => esc_html__('06. Header Five Options','attorg'),
		'parent' => 'attorg_main_panel',
		'priority' => 11,
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Info Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_05_info_bar_bg_color',
				'type' => 'color',
				'title' => esc_html__('Info Bar Background Color','attorg'),
				'default' => 'transparent'
			),
			array(
				'id' => 'header_05_info_bar_text_color',
				'type' => 'color',
				'title' => esc_html__('Info Bar Text Color','attorg'),
				'default' => 'rgba(255, 255, 255, 0.8)'
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Nav Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_05_nav_bar_bg_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Background Color','attorg'),
				'default' => 'transparent'
			),
			array(
				'id' => 'header_05_nav_bar_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Text Color','attorg'),
				'default' => 'rgba(255, 255, 255, 0.8)'
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Dropdown Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_05_dropdown_bg_color',
				'type' => 'color',
				'title' => esc_html__('Dropdown Background Color','attorg'),
				'default' => '#ffffff'
			),
			array(
				'id' => 'header_05_dropdown_color',
				'type' => 'color',
				'title' => esc_html__('Dropdown Text Color','attorg'),
				'default' => '#878a95'
			)
		)
	));
	/* header six */
	CSF::createSection( $prefix.'_customize_options', array(
		'title'  => esc_html__('07. Header Six Options','attorg'),
		'parent' => 'attorg_main_panel',
		'priority' => 11,
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Info Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_06_info_bar_bg_color',
				'type' => 'color',
				'title' => esc_html__('Info Bar Background Color','attorg'),
				'default' => '#F7F7F7'
			),
			array(
				'id' => 'header_06_info_bar_text_color',
				'type' => 'color',
				'title' => esc_html__('Info Bar Text Color','attorg'),
				'default' => '#878a95'
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Nav Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_06_nav_bar_bg_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Background Color','attorg'),
				'default' => '#242930'
			),
			array(
				'id' => 'header_06_nav_bar_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Text Color','attorg'),
				'default' => 'rgba(255, 255, 255, 0.8)'
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Nav Bar Button Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_06_nav_bar_button_bg_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Button Background Color','attorg'),
				'default' => '#161a1d'
			),
			array(
				'id' => 'header_06_nav_bar_button_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Button Color','attorg'),
				'default' => '#fff'
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Dropdown Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_06_dropdown_bg_color',
				'type' => 'color',
				'title' => esc_html__('Dropdown Background Color','attorg'),
				'default' => '#ffffff'
			),
			array(
				'id' => 'header_06_dropdown_color',
				'type' => 'color',
				'title' => esc_html__('Dropdown Text Color','attorg'),
				'default' => '#878a95'
			)
		)
	));
	/* header seven */
	CSF::createSection( $prefix.'_customize_options', array(
		'title'  => esc_html__('08. Header Seven Options','attorg'),
		'parent' => 'attorg_main_panel',
		'priority' => 11,
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Info Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_07_info_bar_bg_color',
				'type' => 'color',
				'title' => esc_html__('Info Bar Background Color','attorg'),
				'default' => 'transparent'
			),
			array(
				'id' => 'header_07_info_bar_text_color',
				'type' => 'color',
				'title' => esc_html__('Info Bar Text Color','attorg'),
				'default' => 'rgba(255,255,255.8)'
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Nav Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_07_nav_bar_bg_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Background Color','attorg'),
				'default' => 'transparent'
			),
			array(
				'id' => 'header_07_nav_bar_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Text Color','attorg'),
				'default' => 'rgba(255, 255, 255, 0.8)'
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Nav Bar Button Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_07_nav_bar_button_color',
				'type' => 'color',
				'title' => esc_html__('Nav Bar Button Color','attorg'),
				'default' => '#fff'
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Dropdown Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_07_dropdown_bg_color',
				'type' => 'color',
				'title' => esc_html__('Dropdown Background Color','attorg'),
				'default' => '#ffffff'
			),
			array(
				'id' => 'header_07_dropdown_color',
				'type' => 'color',
				'title' => esc_html__('Dropdown Text Color','attorg'),
				'default' => '#878a95'
			)
		)
	));
/*-------------------------------------
	  ** Theme Sidebar Options
  -------------------------------------*/
	CSF::createSection($prefix.'_customize_options',array(
		'title' => esc_html__('07. Sidebar','attorg'),
		'priority' => 13,
		'parent' => 'attorg_main_panel',
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Sidebar Options','attorg').'</h3>'
			),
			array(
				'id' => 'sidebar_widget_border_color',
				'type' => 'color',
				'title' => esc_html__('Sidebar Widget Border Color','attorg'),
				'default' => '#fafafa'
			),
			array(
				'id' => 'sidebar_widget_title_color',
				'type' => 'color',
				'title' => esc_html__('Sidebar Widget Title Color','attorg'),
				'default' => '#242424'
			),
			array(
				'id' => 'sidebar_widget_text_color',
				'type' => 'color',
				'title' => esc_html__('Sidebar Widget Text Color','attorg'),
				'default' => '#777777'
			),
		)
	));
	/*-------------------------------------
		** Theme Footer Options
	-------------------------------------*/
	CSF::createSection($prefix.'_customize_options',array(
		'title' => esc_html__('08. Footer','attorg'),
		'priority' => 14,
		'parent' => 'attorg_main_panel',
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Footer Options','attorg').'</h3>'
			),
			array(
				'id' => 'footer_area_bg_color',
				'type' => 'color',
				'title' => esc_html__('Footer Background Color','attorg'),
				'default' => '#1d2228',

			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Footer Widget Options','attorg').'</h3>'
			),
			array(
				'id' => 'footer_widget_title_color',
				'type' => 'color',
				'title' => esc_html__('Footer Widget Title Color','attorg'),
				'default' => '#ffffff'
			),
			array(
				'id' => 'footer_widget_text_color',
				'type' => 'color',
				'title' => esc_html__('Footer Widget Text Color','attorg'),
				'default' => 'rgba(255, 255, 255, 0.7)'
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Copyright Area Options','attorg').'</h3>'
			),
			array(
				'id' => 'copyright_area_bg_color',
				'type' => 'color',
				'title' => esc_html__('Copyright Area Background Color','attorg'),
				'default' => '#161a1e'
			),
			array(
				'id' => 'copyright_area_text_color',
				'type' => 'color',
				'title' => esc_html__('Copyright Area Text Color','attorg'),
				'default' => 'rgba(255, 255, 255, 0.6)'
			),
		)
	));

}//endif