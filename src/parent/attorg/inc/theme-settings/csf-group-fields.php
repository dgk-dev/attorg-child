<?php
/*
 * @package Attorg
 * @since 1.0.0
 * */

if ( !defined('ABSPATH') ){
	exit(); // exit if access directly
}


if ( !class_exists('Attorg_Group_Fields') ){

	class Attorg_Group_Fields{
		/*
			* $instance
			* @since 1.0.0
			* */
		private static $instance;
		/*
		* construct()
		* @since 1.0.0
		* */
		public function __construct() {

		}
		/*
	   * getInstance()
	   * @since 1.0.0
	   * */
		public static function getInstance(){
			if ( null == self::$instance ){
				self::$instance = new self();
			}
			return self::$instance;
		}

		/**
		 * page layout options
		 * @since 1.0.0
		 * */
		public static function page_layout(){
			$fields = array(
				array(
					'type'    => 'subheading',
					'content' => esc_html__('Page Layouts & Colors Options','attorg'),
				),
				array(
					'id' => 'page_layout',
					'type' => 'image_select',
					'title' => esc_html__('Select Page Layout','attorg'),
					'options' => array(
						'default' => ATTORG_THEME_SETTINGS_IMAGES .'/page/default.png',
						'left-sidebar' => ATTORG_THEME_SETTINGS_IMAGES .'/page/left-sidebar.png',
						'right-sidebar' => ATTORG_THEME_SETTINGS_IMAGES .'/page/right-sidebar.png',
						'blank' => ATTORG_THEME_SETTINGS_IMAGES .'/page/blank.png',
					),
					'default' => 'default'
				),
				array(
					'id' => 'page_bg_color',
					'type' => 'color',
					'title' => esc_html__('Page Background Color','attorg'),
					'default' => '#ffffff'
				),
				array(
					'id' => 'page_content_bg_color',
					'type' => 'color',
					'title' => esc_html__('Page Content Background Color','attorg'),
					'default' => '#ffffff'
				)
			);

			return $fields;
		}

		/**
		 * page container options
		 * @since 1.0.0
		 * */
		public static function  Page_Container_Options($type){
			$fields = array();
			$allowed_html = Attorg()->kses_allowed_html(array('mark'));
			if ('header_options' == $type){
				$fields = array(
					array(
						'type'    => 'subheading',
						'content' => esc_html__('Page Header & Breadcrumb Options','attorg'),
					),
					array(
						'id' => 'navbar_type',
						'title' => esc_html__('Navbar Type','attorg'),
						'type' => 'image_select',
						'options' => array(
							'' => ATTORG_THEME_SETTINGS_IMAGES .'/header/01.png',
							'style-01' => ATTORG_THEME_SETTINGS_IMAGES .'/header/02.png',
							'style-02' => ATTORG_THEME_SETTINGS_IMAGES .'/header/03.png',
							'style-03' => ATTORG_THEME_SETTINGS_IMAGES .'/header/04.png',
							'style-04' => ATTORG_THEME_SETTINGS_IMAGES .'/header/05.png',
							'style-05' => ATTORG_THEME_SETTINGS_IMAGES .'/header/06.png',
							'style-06' => ATTORG_THEME_SETTINGS_IMAGES .'/header/07.png'
						),
						'default' => '',
						'desc' => wp_kses(__('you can set <mark>navbar type</mark> transparent type or solid background.','attorg'),$allowed_html),
					),
					array(
						'id' => 'page_title',
						'type' => 'switcher',
						'title' => esc_html__('Page Title','attorg'),
						'desc' => wp_kses(__('you can set <mark>ON / OFF</mark> to show/hide page title.','attorg'),$allowed_html),
						'text_on' => esc_html__('Yes','attorg'),
						'text_off' => esc_html__('No','attorg'),
						'default' => true
					),
					array(
						'id' => 'page_breadcrumb',
						'type' => 'switcher',
						'title' => esc_html__('Page Breadcrumb','attorg'),
						'desc' => wp_kses(__('you can set <mark>ON / OFF</mark> to show/hide page breadcrumb.','attorg'),$allowed_html),
						'text_on' => esc_html__('Yes','attorg'),
						'text_off' => esc_html__('No','attorg'),
						'default' => true
					),
				);
			}elseif ('container_options' == $type){
				$fields = array(
					array(
						'type'    => 'subheading',
						'content' => esc_html__('Page Width & Padding Options','attorg'),
					),
					array(
						'id' => 'page_container',
						'type' => 'switcher',
						'title' => esc_html__('Page Full Width','attorg'),
						'desc' => wp_kses(__('you can set <mark>ON / OFF</mark> to set page container full width.','attorg'),$allowed_html),
						'text_on' => esc_html__('Yes','attorg'),
						'text_off' => esc_html__('No','attorg'),
						'default' => false
					),
					array(
						'type'    => 'subheading',
						'content' => esc_html__('Page Spacing Options','attorg'),
					),
					array(
						'id' => 'page_spacing_top',
						'title' => esc_html__('Page Spacing Top','attorg'),
						'type' => 'slider',
						'desc' => wp_kses(__('you can set <mark>Padding Top</mark> for page container.','attorg'),$allowed_html),
						'min'     => 0,
						'max'     => 500,
						'step'    => 1,
						'unit'    => 'px',
						'default' => 120,
					),
					array(
						'id' => 'page_spacing_bottom',
						'title' => esc_html__('Page Spacing Bottom','attorg'),
						'type' => 'slider',
						'desc' => wp_kses(__('you can set <mark>Padding Bottom</mark> for page container.','attorg'),$allowed_html),
						'min'     => 0,
						'max'     => 500,
						'step'    => 1,
						'unit'    => 'px',
						'default' => 120,
					),
					array(
						'type'    => 'subheading',
						'content' => esc_html__('Page Content Spacing Options','attorg'),
					),
					array(
						'id' => 'page_content_spacing',
						'type' => 'switcher',
						'title' => esc_html__('Page Content Spacing','attorg'),
						'desc' => wp_kses(__('you can set <mark>ON / OFF</mark> to set page content spacing.','attorg'),$allowed_html),
						'text_on' => esc_html__('Yes','attorg'),
						'text_off' => esc_html__('No','attorg'),
						'default' => false
					),
					array(
						'id' => 'page_content_spacing_top',
						'title' => esc_html__('Page Spacing Bottom','attorg'),
						'type' => 'slider',
						'desc' => wp_kses(__('you can set <mark>Padding Top</mark> for page content area.','attorg'),$allowed_html),
						'min'     => 0,
						'max'     => 500,
						'step'    => 1,
						'unit'    => 'px',
						'default' => 0,
						'dependency' => array('page_content_spacing' ,'==','true')
					),
					array(
						'id' => 'page_content_spacing_bottom',
						'title' => esc_html__('Page Spacing Bottom','attorg'),
						'type' => 'slider',
						'desc' => wp_kses(__('you can set <mark>Padding Bottom</mark> for page content area.','attorg'),$allowed_html),
						'min'     => 0,
						'max'     => 500,
						'step'    => 1,
						'unit'    => 'px',
						'default' => 0,
						'dependency' => array('page_content_spacing' ,'==','true')
					),
					array(
						'id' => 'page_content_spacing_left',
						'title' => esc_html__('Page Spacing Left','attorg'),
						'type' => 'slider',
						'desc' => wp_kses(__('you can set <mark>Padding Left</mark> for page content area.','attorg'),$allowed_html),
						'min'     => 0,
						'max'     => 500,
						'step'    => 1,
						'unit'    => 'px',
						'default' => 0,
						'dependency' => array('page_content_spacing' ,'==','true')
					),
					array(
						'id' => 'page_content_spacing_right',
						'title' => esc_html__('Page Spacing Right','attorg'),
						'type' => 'slider',
						'desc' => wp_kses(__('you can set <mark>Padding Right</mark> for page content area.','attorg'),$allowed_html),
						'min'     => 0,
						'max'     => 500,
						'step'    => 1,
						'unit'    => 'px',
						'default' => 0,
						'dependency' => array('page_content_spacing' ,'==','true')
					),
				);
			}

			return $fields;
		}
		/**
		 * page layout options
		 * */
		public static function page_layout_options($title,$prefix){
			$allowed_html = Attorg()->kses_allowed_html(array('mark'));
			$fields = array(
				array(
					'type' => 'subheading',
					'content' => '<h3>'.$title.esc_html__(' Page Options','attorg').'</h3>',
				),
				array(
					'id' => $prefix.'_layout',
					'type' => 'image_select',
					'title' => esc_html__('Select Page Layout','attorg'),
					'options' => array(
						'right-sidebar' => ATTORG_THEME_SETTINGS_IMAGES .'/page/right-sidebar.png',
						'left-sidebar' => ATTORG_THEME_SETTINGS_IMAGES .'/page/left-sidebar.png',
						'no-sidebar' => ATTORG_THEME_SETTINGS_IMAGES .'/page/no-sidebar.png',
					),
					'default' => 'right-sidebar'
				),
				array(
					'id' => $prefix.'_bg_color',
					'type' => 'color',
					'title' => esc_html__('Page Background Color','attorg'),
					'default' => '#ffffff'
				),
				array(
					'id' => $prefix.'_spacing_top',
					'title' => esc_html__('Page Spacing Top','attorg'),
					'type' => 'slider',
					'desc' => wp_kses(__('you can set <mark>Padding Top</mark> for page content area.','attorg'),$allowed_html),
					'min'     => 0,
					'max'     => 500,
					'step'    => 1,
					'unit'    => 'px',
					'default' => 120,
				),
				array(
					'id' => $prefix.'_spacing_bottom',
					'title' => esc_html__('Page Spacing Bottom','attorg'),
					'type' => 'slider',
					'desc' => wp_kses(__('you can set <mark>Padding Bottom</mark> for page content area.','attorg'),$allowed_html),
					'min'     => 0,
					'max'     => 500,
					'step'    => 1,
					'unit'    => 'px',
					'default' => 120,
				),
			);

			return $fields;
		}

		/**
		 * Post meta
		 * @since 1.0.0
		 * */
		public static function post_meta($prefix,$title){
			$allowed_html = Attorg()->kses_allowed_html(array('mark'));
			$fields = array(
				array(
					'type' => 'subheading',
					'content' => '<h3>'.$title.esc_html__(' Post Options','attorg').'</h3>',
				),
				array(
					'id' => $prefix.'_posted_by',
					'type' => 'switcher',
					'title' => esc_html__('Posted By','attorg'),
					'desc' => wp_kses(__('you can set <mark>ON / OFF</mark> to show / hide posted by.','attorg'),$allowed_html),
					'text_on' => esc_html__('Yes','attorg'),
					'text_off' => esc_html__('No','attorg'),
					'default' => true
				),
				array(
					'id' => $prefix.'_posted_on',
					'type' => 'switcher',
					'title' => esc_html__('Posted On','attorg'),
					'desc' => wp_kses(__('you can set <mark>ON / OFF</mark> to show / hide posted on.','attorg'),$allowed_html),
					'text_on' => esc_html__('Yes','attorg'),
					'text_off' => esc_html__('No','attorg'),
					'default' => true
				)
			);

			if ( 'blog_post' == $prefix){
				$fields[] = array(
					'id' => $prefix.'_posted_cat',
					'type' => 'switcher',
					'title' => esc_html__('Posted Category','attorg'),
					'desc' => wp_kses(__('you can set <mark>ON / OFF</mark> to show / hide posted category.','attorg'),$allowed_html),
					'text_on' => esc_html__('Yes','attorg'),
					'text_off' => esc_html__('No','attorg'),
					'default' => true
				);
				$fields[] =  array(
					'id' => $prefix.'_readmore_btn',
					'type' => 'switcher',
					'title' => esc_html__('Read More Button','attorg'),
					'desc' => wp_kses(__('you can set <mark>ON / OFF</mark> to show / hide read more button.','attorg'),$allowed_html),
					'text_on' => esc_html__('Yes','attorg'),
					'text_off' => esc_html__('No','attorg'),
					'default' => true
				);
				$fields[] =  array(
					'id' => $prefix.'_readmore_btn_text',
					'type' => 'text',
					'title' => esc_html__('Read More Text','attorg'),
					'desc' => wp_kses(__('you can set read more <mark>button text</mark> to button text.','attorg'),$allowed_html),
					'default' => esc_html__('Read More','attorg'),
					'dependency' => array($prefix.'_readmore_btn' ,'==','true')
				);
				$fields[] =  array(
					'id' => $prefix.'_excerpt_more',
					'type' => 'text',
					'title' => esc_html__('Excerpt More','attorg'),
					'desc' => wp_kses(__('you can set read more <mark>button text</mark> to button text.','attorg'),$allowed_html),
					'attributes' => array(
						'placeholder' => esc_html__('....','attorg')
					)
				);
				$fields[] =  array(
					'id' => $prefix.'_excerpt_length',
					'type' => 'select',
					'options' => array(
						'25' => esc_html__('Short','attorg'),
						'55' => esc_html__('Regular','attorg'),
						'100' => esc_html__('Long','attorg'),
					),
					'title' => esc_html__('Excerpt Length','attorg'),
					'desc' => wp_kses(__('you can set <mark> excerpt length</mark> for post.','attorg'),$allowed_html),
				);
			}elseif('blog_single_post' == $prefix){

				$fields[] =array(
					'id' => $prefix.'_posted_category',
					'type' => 'switcher',
					'title' => esc_html__('Posted Category','attorg'),
					'desc' => wp_kses(__('you can set <mark>ON / OFF</mark> to show / hide posted category.','attorg'),$allowed_html),
					'text_on' => esc_html__('Yes','attorg'),
					'text_off' => esc_html__('No','attorg'),
					'default' => true
				);
				$fields[] =  array(
					'id' => $prefix.'_posted_tag',
					'type' => 'switcher',
					'title' => esc_html__('Posted Tags','attorg'),
					'desc' => wp_kses(__('you can set <mark>ON / OFF</mark> to show / hide post tags.','attorg'),$allowed_html),
					'text_on' => esc_html__('Yes','attorg'),
					'text_off' => esc_html__('No','attorg'),
					'default' => true
				);
				$fields[] =  array(
					'id' => $prefix.'_posted_share',
					'type' => 'switcher',
					'title' => esc_html__('Post Share','attorg'),
					'desc' => wp_kses(__('you can set <mark>ON / OFF</mark> to show / hide post share.','attorg'),$allowed_html),
					'text_on' => esc_html__('Yes','attorg'),
					'text_off' => esc_html__('No','attorg'),
					'default' => true
				);
			}

			return $fields;
		}

	}//end class

}//end if

