<?php
/*
 * Theme Metabox Options
 * @package Attorg
 * @since 1.0.0
 * */

if ( !defined('ABSPATH') ){
	exit(); // exit if access directly
}

if ( class_exists('CSF') ){

	$allowed_html = Attorg()->kses_allowed_html(array('mark'));

	$prefix = 'attorg';
	/*-------------------------------------
		Post Format Options
	-------------------------------------*/
	CSF::createMetabox($prefix.'_post_video_options',array(
		'title' => esc_html__('Video Post Format Options','attorg'),
		'post_type' => 'post',
		'post_formats' => 'video'
	));
	CSF::createSection($prefix.'_post_video_options',array(
		'fields' => array(
			array(
				'id' => 'video_url',
				'type' => 'text',
				'title' => esc_html__('Enter Video URL','attorg'),
				'desc' => wp_kses(__('enter <mark>video url</mark> to show in frontend','attorg'),$allowed_html)
			)
		)
	));
	CSF::createMetabox($prefix.'_post_gallery_options',array(
		'title' => esc_html__('Gallery Post Format Options','attorg'),
		'post_type' => 'post',
		'post_formats' => 'gallery'
	));
	CSF::createSection($prefix.'_post_gallery_options',array(
		'fields' => array(
			array(
				'id' => 'gallery_images',
				'type' => 'gallery',
				'title' => esc_html__('Select Gallery Photos','attorg'),
				'desc' => wp_kses(__('select <mark>gallery photos</mark> to show in frontend','attorg'),$allowed_html)
			)
		)
	));
	/*-------------------------------------
	  Page Container Options
  -------------------------------------*/
	CSF::createMetabox($prefix.'_page_container_options',array(
		'title' => esc_html__('Page Options','attorg'),
		'post_type' => array('page'),
	));
	CSF::createSection($prefix.'_page_container_options',array(
		'title' => esc_html__('Layout & Colors','attorg'),
		'icon' => 'fa fa-columns',
		'fields' => Attorg_Group_Fields::page_layout()
	));
	CSF::createSection($prefix.'_page_container_options',array(
		'title' => esc_html__('Header & Breadcrumb','attorg'),
		'icon' => 'fa fa-header',
		'fields' => Attorg_Group_Fields::Page_Container_Options('header_options')
	));
	CSF::createSection($prefix.'_page_container_options',array(
		'title' => esc_html__('Width & Padding','attorg'),
		'icon' => 'fa fa-file-o',
		'fields' => Attorg_Group_Fields::Page_Container_Options('container_options')
	));
	/*-------------------------------------
	 Practice Area Options
  -------------------------------------*/
	CSF::createMetabox($prefix.'_practice_area_options',array(
		'title' => esc_html__('Practice Options','attorg'),
		'post_type' => array('practice-area'),
		'priority'           => 'high',
	));
	CSF::createSection($prefix.'_practice_area_options',array(
		'fields' => array(
			array(
				'id' => 'icon',
				'type' => 'icon',
				'title' => esc_html__('Icon','attorg'),
			)
		)
	));
	/*-------------------------------------
	 Attorney Options
  -------------------------------------*/
	CSF::createMetabox($prefix.'_attorney_options',array(
		'title' => esc_html__('Attorney Options','attorg'),
		'post_type' => array('attorney'),
		'priority' => 'high'
	));
	CSF::createSection($prefix.'_attorney_options',array(
		'title' => esc_html__('Attorney Info','attorg'),
		'id' => 'attorg-info',
		'fields' => array(
			array(
				'id' => 'designation',
				'type' => 'text',
				'title' => esc_html__('Designation','attorg'),
			),
			array(
				'id' => 'location',
				'type' => 'text',
				'title' => esc_html__('Location','attorg'),
			),
			array(
				'id' => 'phone',
				'type' => 'text',
				'title' => esc_html__('Phone','attorg'),
			),
			array(
				'id' => 'email',
				'type' => 'text',
				'title' => esc_html__('Email','attorg'),
			),
			array(
				'id' => 'languages',
				'type' => 'text',
				'title' => esc_html__('Languages','attorg'),
			),
		)
	));
	CSF::createSection($prefix.'_attorney_options',array(
		'title' => esc_html__('Social Info','attorg'),
		'id' => 'social-info',
		'fields' => array(
			array(
				'id'     => 'social-icons',
				'type'   => 'repeater',
				'title'  => esc_html__('Social Info','attorg'),
				'fields' => array(

					array(
						'id'    => 'title',
						'type'  => 'text',
						'title' => esc_html__('Title','attorg')
					),
					array(
						'id'    => 'icon',
						'type'  => 'icon',
						'title' => esc_html__('Icon','attorg')
					),
					array(
						'id'    => 'url',
						'type'  => 'text',
						'title' => esc_html__('URL','attorg')
					),

				),
			),
		)
	));
	CSF::createSection($prefix.'_attorney_options',array(
		'title' => esc_html__('Contact Info','attorg'),
		'id' => 'contact-info',
		'fields' => array(
			array(
				'id' => 'contact_shortcode',
				'type' => 'textarea',
				'title' => esc_html__('Contact Form Shortcode','attorg'),
			),
		)
	));

}//endif