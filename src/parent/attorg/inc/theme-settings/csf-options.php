<?php
/**
 * Theme Options
 * @Packange Attorg
 * @since 1.0.0
 */
if ( !defined('ABSPATH') ){
	exit(); // exit if access directly
}
// Control core classes for avoid errors
if( class_exists('CSF') ) {

	$allowed_html = Attorg()->kses_allowed_html(array('mark'));
	$prefix = 'attorg';
	// Create options
	CSF::createOptions( $prefix.'_theme_options', array(
		'menu_title' => esc_html__('Theme Options','attorg'),
		'menu_slug'  => 'attorg-theme-settings',
		'menu_parent'  => 'attorg_theme_options',
		'menu_type' => 'submenu',
		'footer_credit' => ' ',
		'menu_icon' => 'fa fa-filter',
		'show_footer' => false,
		'enqueue_webfont' => false,
		'show_search'        => false,
		'show_reset_all'     => true,
		'show_reset_section' => false,
		'show_all_options'   => false,
		'theme' => 'dark',
		'framework_title' =>  Attorg()->get_theme_info('name').'<a href="'.Attorg()->get_theme_info('author_uri').'" class="author_link">'.'<span>'.esc_html__('Author - ','attorg').' </span>'.Attorg()->get_theme_info('author').'</a>'
	) );

	/*-------------------------------------------------------
		** General  Options
	--------------------------------------------------------*/
	CSF::createSection( $prefix.'_theme_options', array(
		'title'  => esc_html__('General','attorg'),
		'id' => 'general_options',
		'icon'  => 'fa fa-wrench',
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Preloader Options','attorg').'</h3>'
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Default Header Options','attorg').'</h3>'
			),
			array(
				'id' => 'default_navbar_type',
				'title' => esc_html__('Default Navbar Type','attorg'),
				'type' => 'image_select',
				'options' => array(
					'' => ATTORG_THEME_SETTINGS_IMAGES .'/header/01.png',
					'style-01' => ATTORG_THEME_SETTINGS_IMAGES .'/header/02.png',
					'style-02' => ATTORG_THEME_SETTINGS_IMAGES .'/header/03.png',
					'style-03' => ATTORG_THEME_SETTINGS_IMAGES .'/header/04.png',
					'style-04' => ATTORG_THEME_SETTINGS_IMAGES .'/header/05.png',
					'style-05' => ATTORG_THEME_SETTINGS_IMAGES .'/header/06.png',
					'style-06' => ATTORG_THEME_SETTINGS_IMAGES .'/header/07.png'
				),
				'default' => '',
				'desc' => wp_kses(__('you can set <mark>navbar type</mark> transparent type or solid background.','attorg'),$allowed_html),
			),
			array(
				'id' => 'preloader_enable',
				'title' => esc_html__('Preloader','attorg'),
				'type' => 'switcher',
				'desc' => wp_kses(__('you can set <mark>Yes / No</mark> to enable/disable preloader','attorg'),$allowed_html),
				'default' => true,
			),
			array(
				'id' => 'preloader_bg_color',
				'title' => esc_html__('Background Color','attorg'),
				'type' => 'color',
				'default' => '#ffffff',
				'desc' => wp_kses(__('you can set <mark>overlay color</mark> for breadcrumb background image','attorg'),$allowed_html),
				'dependency' => array('preloader_enable','==','true')
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Back Top Options','attorg').'</h3>'
			),
			array(
				'id' => 'back_top_enable',
				'title' => esc_html__('Back Top','attorg'),
				'type' => 'switcher',
				'desc' => wp_kses(__('you can set <mark>Yes / No</mark> to show/hide back to top','attorg'),$allowed_html),
				'default' => true,
			),
			array(
				'id' => 'back_top_icon',
				'title' => esc_html__('Back Top Icon','attorg'),
				'type' => 'icon',
				'default' => 'fa fa-angle-up',
				'desc' => wp_kses(__('you can set <mark>icon</mark> for back to top.','attorg'),$allowed_html),
				'dependency' => array('back_top_enable','==','true')
			),
		)
	) );
	/*-------------------------------------------------------
	   ** Entire Site Header  Options
   --------------------------------------------------------*/
	CSF::createSection($prefix.'_theme_options',array(
		'id' => 'headers_settings',
		'title' => esc_html__('Headers Settings','attorg'),
		'icon' => 'fa fa-header'
	));
	/* Header Style 01 */
	CSF::createSection( $prefix.'_theme_options', array(
		'title'  => esc_html__('Header One','attorg'),
		'id' => 'theme_header_one_options',
		'icon' => 'fa fa-image',
		'parent' => 'headers_settings',
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Logo Options','attorg').'</h3>'
			),
			array(
				'id'      => 'header_one_logo',
				'type'    => 'media',
				'title'   => esc_html__('Logo','attorg'),
				'library' => 'image',
				'desc' => wp_kses(__('you can upload <mark> logo</mark> here it will overwrite customizer uploaded logo','attorg'),$allowed_html),
			)
		)
	));
	/* Header Style 02 */
	CSF::createSection( $prefix.'_theme_options', array(
		'title'  => esc_html__('Header Two','attorg'),
		'id' => 'theme_header_two_options',
		'icon' => 'fa fa-image',
		'parent' => 'headers_settings',
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Logo Options','attorg').'</h3>'
			),
			array(
				'id'      => 'header_two_logo',
				'type'    => 'media',
				'title'   => esc_html__('Logo','attorg'),
				'library' => 'image',
				'desc' => wp_kses(__('you can upload <mark> logo</mark> here it will overwrite customizer uploaded logo','attorg'),$allowed_html),
			),
			
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Info Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_two_left_side_content',
				'type' => 'textarea',
				'title' => esc_html__('Left Side Content','attorg'),
				'desc' => wp_kses(__('you can set <mark> shortcode</mark> for header two info bar left side','attorg'),$allowed_html),
				'shortcoder'=> 'attorg_shortcodes'
			),
			array(
				'id' => 'header_two_info_btn',
				'type' => 'switcher',
				'title' => esc_html__('Button','attorg'),
				'default' => true,
				'desc' => wp_kses(__('you can <mark> show/hide</mark> button of header two info bar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_two_info_btn_text',
				'type' => 'text',
				'title' => esc_html__('Button Text','attorg'),
				'default' => esc_html__('Free Consultation','attorg'),
				'desc' => wp_kses(__('you can set <mark> button</mark> text for header two info bar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_two_info_btn_link',
				'type' => 'text',
				'title' => esc_html__('Button Link','attorg'),
				'default' => '#',
				'desc' => wp_kses(__('you can set <mark> button</mark> text for header two info bar','attorg'),$allowed_html),
			),
		)
	));
	/* Header Style 03 */
	CSF::createSection( $prefix.'_theme_options', array(
		'title'  => esc_html__('Header Three','attorg'),
		'id' => 'theme_header_three_options',
		'icon' => 'fa fa-image',
		'parent' => 'headers_settings',
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Logos Options','attorg').'</h3>'
			),
			array(
				'id'      => 'header_three_logo',
				'type'    => 'media',
				'title'   => esc_html__('Logo','attorg'),
				'library' => 'image',
				'desc' => wp_kses(__('you can upload <mark> logo</mark> here it will overwrite customizer uploaded logo','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_three_nav_btn',
				'type' => 'switcher',
				'title' => esc_html__('Button','attorg'),
				'default' => true,
				'desc' => wp_kses(__('you can <mark> show/hide</mark> button of header three navbar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_three_nav_btn_title',
				'type' => 'text',
				'title' => esc_html__('Button Title','attorg'),
				'default' => esc_html__('(888) 123 4567','attorg'),
				'desc' => wp_kses(__('you can set <mark> button</mark> title for header three navbar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_three_nav_btn_link',
				'type' => 'text',
				'title' => esc_html__('Button Link','attorg'),
				'default' => '#',
				'desc' => wp_kses(__('you can set <mark> button</mark> text for header three navbar','attorg'),$allowed_html),
			),
		)
	));
	/* Header Style 04 */
	CSF::createSection( $prefix.'_theme_options', array(
		'title'  => esc_html__('Header Four','attorg'),
		'id' => 'theme_header_four_options',
		'icon' => 'fa fa-image',
		'parent' => 'headers_settings',
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Logo Options','attorg').'</h3>'
			),
			array(
				'id'      => 'header_four_logo',
				'type'    => 'media',
				'title'   => esc_html__('Logo','attorg'),
				'library' => 'image',
				'desc' => wp_kses(__('you can upload <mark> logo</mark> here it will overwrite customizer uploaded logo','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_four_nav_btn',
				'type' => 'switcher',
				'title' => esc_html__('Button','attorg'),
				'default' => true,
				'desc' => wp_kses(__('you can <mark> show/hide</mark> button of header four navbar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_four_nav_btn_title',
				'type' => 'text',
				'title' => esc_html__('Button Title','attorg'),
				'default' => esc_html__('(888) 123 4567','attorg'),
				'desc' => wp_kses(__('you can set <mark> button</mark> title for header four navbar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_four_nav_btn_link',
				'type' => 'text',
				'title' => esc_html__('Button Link','attorg'),
				'default' => '#',
				'desc' => wp_kses(__('you can set <mark> button</mark> text for header four navbar','attorg'),$allowed_html),
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Info Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_four_info_bar_content',
				'type' => 'repeater',
				'title' => esc_html__('Shortcodes','attorg'),
				'desc' => wp_kses(__('you can set <mark> shortcode</mark> for header two info bar left side','attorg'),$allowed_html),
				'fields' => array(
					array(
						'id' => 'header_four_info_bar_title',
						'title' => esc_html__('Title','attorg'),
						'type' => 'text',
						'desc' => wp_kses(__('enter title for info bar <mark> shortcode</mark> it will not show in frontend','attorg'),$allowed_html),
					),
					array(
						'id' => 'header_four_info_bar_content_area',
						'title' => esc_html__('Content Area','attorg'),
						'type' => 'select',
						'options' => array(
							'left-contnet' => esc_html__('Left Content','attorg'),
							'right-contnet' => esc_html__('Right Content','attorg'),
						),
						'desc' => wp_kses(__('set content for info bar <mark> shortcode</mark>.','attorg'),$allowed_html),
					),
					array(
						'id' => 'header_four_info_bar_shortcodes',
						'type' => 'textarea',
						'title' => esc_html__('Shortcodes','attorg'),
						'desc' => wp_kses(__('you can set <mark> shortcode</mark> for header two info bar left side','attorg'),$allowed_html),
						'shortcoder'=> 'attorg_shortcodes'
					),
				)
			)
		)
	));
	/* Header Style 05 */
	CSF::createSection( $prefix.'_theme_options', array(
		'title'  => esc_html__('Header Five','attorg'),
		'id' => 'theme_header_five_options',
		'icon' => 'fa fa-image',
		'parent' => 'headers_settings',
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Logo Options','attorg').'</h3>'
			),
			array(
				'id'      => 'header_five_logo',
				'type'    => 'media',
				'title'   => esc_html__('Logo','attorg'),
				'library' => 'image',
				'desc' => wp_kses(__('you can upload <mark> logo</mark> here it will overwrite customizer uploaded logo','attorg'),$allowed_html),
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Info Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_five_left_side_content',
				'type' => 'textarea',
				'title' => esc_html__('Left Side Content','attorg'),
				'desc' => wp_kses(__('you can set <mark> shortcode</mark> for header two info bar left side','attorg'),$allowed_html),
				'shortcoder'=> 'attorg_shortcodes'
			),
			array(
				'id' => 'header_five_info_btn',
				'type' => 'switcher',
				'title' => esc_html__('Button','attorg'),
				'default' => true,
				'desc' => wp_kses(__('you can <mark> show/hide</mark> button of header two info bar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_five_info_btn_text',
				'type' => 'text',
				'title' => esc_html__('Button Text','attorg'),
				'default' => esc_html__('Free Consultation','attorg'),
				'desc' => wp_kses(__('you can set <mark> button</mark> text for header two info bar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_five_info_btn_link',
				'type' => 'text',
				'title' => esc_html__('Button Link','attorg'),
				'default' => '#',
				'desc' => wp_kses(__('you can set <mark> button</mark> text for header two info bar','attorg'),$allowed_html),
			),
		)
	));
	/* Header Style 06 */
	CSF::createSection( $prefix.'_theme_options', array(
		'title'  => esc_html__('Header Six','attorg'),
		'id' => 'theme_header_six_options',
		'icon' => 'fa fa-image',
		'parent' => 'headers_settings',
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Navbar Options','attorg').'</h3>'
			),
			array(
				'id'      => 'header_six_logo',
				'type'    => 'media',
				'title'   => esc_html__('Logo','attorg'),
				'library' => 'image',
				'desc' => wp_kses(__('you can upload <mark> logo</mark> here it will overwrite customizer uploaded logo','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_six_nav_btn',
				'type' => 'switcher',
				'title' => esc_html__('Button','attorg'),
				'default' => true,
				'desc' => wp_kses(__('you can <mark> show/hide</mark> button of header six navbar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_six_nav_btn_icon',
				'type' => 'icon',
				'title' => esc_html__('Button Icon','attorg'),
				'default' => 'flaticon-call',
				'desc' => wp_kses(__('you can set <mark> button </mark> icon for header six navbar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_six_nav_btn_title',
				'type' => 'text',
				'title' => esc_html__('Button Title','attorg'),
				'default' => esc_html__('(888) 123 4567','attorg'),
				'desc' => wp_kses(__('you can set <mark> button</mark> title for header six navbar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_six_nav_btn_subtitle',
				'type' => 'text',
				'title' => esc_html__('Button Subtitle','attorg'),
				'default' => esc_html__('Call Us For Query','attorg'),
				'desc' => wp_kses(__('you can set <mark> button</mark> subtitle for header six navbar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_six_nav_btn_link',
				'type' => 'text',
				'title' => esc_html__('Button Link','attorg'),
				'default' => '#',
				'desc' => wp_kses(__('you can set <mark> button</mark> text for header six navbar','attorg'),$allowed_html),
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Info Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_six_left_side_content',
				'type' => 'textarea',
				'title' => esc_html__('Left Side Content','attorg'),
				'desc' => wp_kses(__('you can set <mark> shortcode</mark> for header six info bar left side','attorg'),$allowed_html),
				'shortcoder'=> 'attorg_shortcodes'
			),
			array(
				'id' => 'header_six_info_btn',
				'type' => 'switcher',
				'title' => esc_html__('Button','attorg'),
				'default' => true,
				'desc' => wp_kses(__('you can <mark> show/hide</mark> button of header six info bar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_six_info_btn_text',
				'type' => 'text',
				'title' => esc_html__('Button Text','attorg'),
				'default' => esc_html__('Free Consultation','attorg'),
				'desc' => wp_kses(__('you can set <mark> button</mark> text for header six info bar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_six_info_btn_link',
				'type' => 'text',
				'title' => esc_html__('Button Link','attorg'),
				'default' => '#',
				'desc' => wp_kses(__('you can set <mark> button</mark> text for header six info bar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_six_right_side_content',
				'type' => 'textarea',
				'title' => esc_html__('Right Side Content','attorg'),
				'desc' => wp_kses(__('you can set <mark> shortcode</mark> for header six info bar right side','attorg'),$allowed_html),
				'shortcoder'=> 'attorg_shortcodes'
			),
		)
	));
	/* Header Style 07 */
	CSF::createSection( $prefix.'_theme_options', array(
		'title'  => esc_html__('Header Seven','attorg'),
		'id' => 'theme_header_seven_options',
		'icon' => 'fa fa-image',
		'parent' => 'headers_settings',
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Navbar Options','attorg').'</h3>'
			),
			array(
				'id'      => 'header_seven_logo',
				'type'    => 'media',
				'title'   => esc_html__('Logo','attorg'),
				'library' => 'image',
				'desc' => wp_kses(__('you can upload <mark> logo</mark> here it will overwrite customizer uploaded logo','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_seven_nav_btn',
				'type' => 'switcher',
				'title' => esc_html__('Button','attorg'),
				'default' => true,
				'desc' => wp_kses(__('you can <mark> show/hide</mark> button of header seven navbar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_seven_nav_btn_icon',
				'type' => 'icon',
				'title' => esc_html__('Button Icon','attorg'),
				'default' => 'flaticon-call',
				'desc' => wp_kses(__('you can set <mark> button </mark> icon for header seven navbar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_seven_nav_btn_title',
				'type' => 'text',
				'title' => esc_html__('Button Title','attorg'),
				'default' => esc_html__('(888) 123 4567','attorg'),
				'desc' => wp_kses(__('you can set <mark> button</mark> title for header seven navbar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_seven_nav_btn_subtitle',
				'type' => 'text',
				'title' => esc_html__('Button Subtitle','attorg'),
				'default' => esc_html__('Call Us For Query','attorg'),
				'desc' => wp_kses(__('you can set <mark> button</mark> subtitle for header seven navbar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_seven_nav_btn_link',
				'type' => 'text',
				'title' => esc_html__('Button Link','attorg'),
				'default' => '#',
				'desc' => wp_kses(__('you can set <mark> button</mark> text for header seven navbar','attorg'),$allowed_html),
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Info Bar Options','attorg').'</h3>'
			),
			array(
				'id' => 'header_seven_left_side_content',
				'type' => 'textarea',
				'title' => esc_html__('Left Side Content','attorg'),
				'desc' => wp_kses(__('you can set <mark> shortcode</mark> for header seven info bar left side','attorg'),$allowed_html),
				'shortcoder'=> 'attorg_shortcodes'
			),
			array(
				'id' => 'header_seven_info_btn',
				'type' => 'switcher',
				'title' => esc_html__('Button','attorg'),
				'default' => true,
				'desc' => wp_kses(__('you can <mark> show/hide</mark> button of header seven info bar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_seven_info_btn_text',
				'type' => 'text',
				'title' => esc_html__('Button Text','attorg'),
				'default' => esc_html__('Free Consultation','attorg'),
				'desc' => wp_kses(__('you can set <mark> button</mark> text for header seven info bar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_seven_info_btn_link',
				'type' => 'text',
				'title' => esc_html__('Button Link','attorg'),
				'default' => '#',
				'desc' => wp_kses(__('you can set <mark> button</mark> text for header seven info bar','attorg'),$allowed_html),
			),
			array(
				'id' => 'header_seven_right_side_content',
				'type' => 'textarea',
				'title' => esc_html__('Right Side Content','attorg'),
				'desc' => wp_kses(__('you can set <mark> shortcode</mark> for header seven info bar right side','attorg'),$allowed_html),
				'shortcoder'=> 'attorg_shortcodes'
			),
		)
	));
	/* Breadcrumb */
	CSF::createSection( $prefix.'_theme_options', array(
		'title'  => esc_html__('Breadcrumb','attorg'),
		'id' => 'breadcrumb_options',
		'icon' => 'fa fa-ellipsis-h',
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Breadcrumb Options','attorg').'</h3>'
			),
			array(
				'id' => 'breadcrumb_enable',
				'title' => esc_html__('Breadcrumb','attorg'),
				'type' => 'switcher',
				'desc' => wp_kses(__('you can set <mark>Yes / No</mark> to show/hide breadcrumb','attorg'),$allowed_html),
				'default' => true,
			),
			array(
				'id' => 'breadcrumb_bg',
				'title' => esc_html__('Background Image','attorg'),
				'type' => 'background',
				'desc' => wp_kses(__('you can set <mark>background</mark> for breadcrumb','attorg'),$allowed_html),
				'default' => array(
					'background-size'               => 'cover',
					'background-position'           => 'center bottom',
					'background-repeat'             => 'no-repeat',
				),
				'background_color' => false,
				'dependency' => array('breadcrumb_enable','==','true')
			)
		)
	));


	/*-------------------------------------------------------
		   ** Footer  Options
	--------------------------------------------------------*/
	CSF::createSection( $prefix.'_theme_options', array(
		'title'  => esc_html__('Footer','attorg'),
		'id' => 'footer_options',
		'icon' => 'fa fa-copyright',
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Footer Options','attorg').'</h3>'
			),
			array(
				'id' => 'footer_spacing',
				'title' => esc_html__('Footer Spacing','attorg'),
				'type' => 'switcher',
				'desc' => wp_kses(__('you can set <mark>Yes / No</mark> to set footer spacing','attorg'),$allowed_html),
				'default' => true,
			),
			array(
				'id' => 'footer_top_spacing',
				'title' => esc_html__('Footer Top Spacing','attorg'),
				'type' => 'slider',
				'desc' => wp_kses(__('you can set <mark>padding</mark> for footer top','attorg'),$allowed_html),
				'min' => 0,
				'max' => 500,
				'step' => 1,
				'unit' => 'px',
				'default' => 100,
				'dependency' => array('footer_spacing' ,'==','true')
			),
			array(
				'id' => 'footer_bottom_spacing',
				'title' => esc_html__('Footer Bottom Spacing','attorg'),
				'type' => 'slider',
				'desc' => wp_kses(__('you can set <mark>padding</mark> for footer bottom','attorg'),$allowed_html),
				'min' => 0,
				'max' => 500,
				'step' => 1,
				'unit' => 'px',
				'default' => 65,
				'dependency' => array('footer_spacing' ,'==','true')
			),
			array(
				'type' => 'subheading',
				'content' =>'<h3>'.esc_html__('Footer Copyright Area Options','attorg').'</h3>'
			),
			array(
				'id' => 'copyright_area_spacing',
				'title' => esc_html__('Copyright Area Spacing','attorg'),
				'type' => 'switcher',
				'desc' => wp_kses(__('you can set <mark>Yes / No</mark> to set copyright area spacing','attorg'),$allowed_html),
				'default' => true,
			),
			array(
				'id' => 'copyright_text',
				'title' => esc_html__('Copyright Area Text','attorg'),
				'type' => 'text',
				'desc' => wp_kses(__('use  <mark>{copy}</mark> for copyright symbol, use <mark>{year}</mark> for current year, ','attorg'),$allowed_html)
			),
			array(
				'id' => 'copyright_area_top_spacing',
				'title' => esc_html__('Copyright Area Top Spacing','attorg'),
				'type' => 'slider',
				'desc' => wp_kses(__('you can set <mark>padding</mark> for copyright area top','attorg'),$allowed_html),
				'min' => 0,
				'max' => 500,
				'step' => 1,
				'unit' => 'px',
				'default' => 20,
				'dependency' => array('copyright_area_spacing' ,'==','true')
			),
			array(
				'id' => 'copyright_area_bottom_spacing',
				'title' => esc_html__('Copyright Area Bottom Spacing','attorg'),
				'type' => 'slider',
				'desc' => wp_kses(__('you can set <mark>padding</mark> for copyright area bottom','attorg'),$allowed_html),
				'min' => 0,
				'max' => 500,
				'step' => 1,
				'unit' => 'px',
				'default' => 20,
				'dependency' => array('copyright_area_spacing' ,'==','true')
			),
		)
	));
	/*-------------------------------------------------------
		  ** Blog  Options
	--------------------------------------------------------*/
	CSF::createSection($prefix.'_theme_options',array(
		'id' => 'blog_settings',
		'title' => esc_html__('Blog Settings','attorg'),
		'icon' => 'fa fa-rss'
	));
	CSF::createSection($prefix.'_theme_options',array(
		'parent' => 'blog_settings',
		'id' => 'blog_post_options',
		'title' => esc_html__('Blog Post','attorg'),
		'icon' => 'fa fa-list-ul',
		'fields' => Attorg_Group_Fields::post_meta('blog_post',esc_html__('Blog Page','attorg'))
	));
	CSF::createSection($prefix.'_theme_options',array(
		'parent' => 'blog_settings',
		'id' => 'blog_single_post_options',
		'title' => esc_html__('Single Post','attorg'),
		'icon' => 'fa fa-list-alt',
		'fields' => Attorg_Group_Fields::post_meta('blog_single_post',esc_html__('Blog Single Page','attorg'))
	));
	/*-------------------------------------------------------
		  ** Pages & templates  Options
   --------------------------------------------------------*/
	CSF::createSection($prefix.'_theme_options',array(
		'id' => 'pages_and_template',
		'title' => esc_html__('Pages Settings','attorg'),
		'icon' => 'fa fa-files-o'
	));
	/*  404 page options */
	CSF::createSection($prefix.'_theme_options',array(
		'id' => '404_page',
		'title' => esc_html__('404 Page','attorg'),
		'parent' => 'pages_and_template',
		'icon' => 'fa fa-exclamation-triangle',
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' => '<h3>'.esc_html__('404 Page Options','attorg').'</h3>',
			),
			array(
				'id' => '404_bg_color',
				'type' => 'color',
				'title' => esc_html__('Page Background Color','attorg'),
				'default' => '#ffffff'
			),
			array(
				'id' =>'404_title',
				'title' => esc_html__('Title','attorg'),
				'type' => 'text',
				'info' => wp_kses(__('you can change <mark>title</mark> of 404 page' ,'attorg'),$allowed_html),
				'attributes' => array('placeholder'=>esc_html__('404','attorg'))
			),
			array(
				'id' => '404_subtitle',
				'title' => esc_html__('Sub Title','attorg'),
				'type' => 'text',
				'info' => wp_kses(__('you can change <mark>sub title</mark> of 404 page' ,'attorg'),$allowed_html),
				'attributes' => array('placeholder'=>esc_html__('Oops! That page can’t be found.','attorg'))
			),
			array(
				'id' => '404_paragraph',
				'title' => esc_html__('Paragraph','attorg'),
				'type' => 'textarea',
				'info' => wp_kses(__('you can change <mark>paragraph</mark> of 404 page' ,'attorg'),$allowed_html),
				'attributes' => array('placeholder'=>esc_html__('It looks like nothing was found at this location. Maybe try one of the links below or a search?','attorg'))
			),
			array(
				'id' => '404_button_text',
				'title' => esc_html__('Button Text','attorg'),
				'type' => 'text',
				'info' => wp_kses(__('you can change <mark>button text</mark> of 404 page' ,'attorg'),$allowed_html),
				'attributes' => array('placeholder'=>esc_html__('back to home','attorg'))
			),
			array(
				'id' => '404_spacing_top',
				'title' => esc_html__('Page Spacing Top','attorg'),
				'type' => 'slider',
				'desc' => wp_kses(__('you can set <mark>Padding Top</mark> for page content area.','attorg'),$allowed_html),
				'min'     => 0,
				'max'     => 500,
				'step'    => 1,
				'unit'    => 'px',
				'default' => 120,
			),
			array(
				'id' => '404_spacing_bottom',
				'title' => esc_html__('Page Spacing Bottom','attorg'),
				'type' => 'slider',
				'desc' => wp_kses(__('you can set <mark>Padding Bottom</mark> for page content area.','attorg'),$allowed_html),
				'min'     => 0,
				'max'     => 500,
				'step'    => 1,
				'unit'    => 'px',
				'default' => 120,
			),
		)
	));
	/*  blog page options */
	CSF::createSection($prefix.'_theme_options',array(
		'id' => 'blog_page',
		'title' => esc_html__('Blog Page','attorg'),
		'parent' => 'pages_and_template',
		'icon' => 'fa fa-indent',
		'fields' => Attorg_Group_Fields::page_layout_options(esc_html__('Blog','attorg'),'blog')
	));
	/*  blog single page options */
	CSF::createSection($prefix.'_theme_options',array(
		'id' => 'blog_single_page',
		'title' => esc_html__('Blog Single Page','attorg'),
		'parent' => 'pages_and_template',
		'icon' => 'fa fa-indent',
		'fields' => Attorg_Group_Fields::page_layout_options(esc_html__('Blog Single','attorg'),'blog_single')
	));
	/*  archive page options */
	CSF::createSection($prefix.'_theme_options',array(
		'id' => 'archive_page',
		'title' => esc_html__('Archive Page','attorg'),
		'parent' => 'pages_and_template',
		'icon' => 'fa fa-archive',
		'fields' => Attorg_Group_Fields::page_layout_options(esc_html__('Archive','attorg'),'archive')
	));
	/*  search page options */
	CSF::createSection($prefix.'_theme_options',array(
		'id' => 'search_page',
		'title' => esc_html__('Search Page','attorg'),
		'parent' => 'pages_and_template',
		'icon' => 'fa fa-search',
		'fields' => Attorg_Group_Fields::page_layout_options(esc_html__('Search','attorg'),'search')
	));
	/*  practice area single options */
	CSF::createSection($prefix.'_theme_options',array(
		'id' => 'practice_single_page',
		'title' => esc_html__('Practice Single Page','attorg'),
		'parent' => 'pages_and_template',
		'icon' => 'fa fa-check-square-o',
		'fields' => Attorg_Group_Fields::page_layout_options(esc_html__('Practice Single Area','attorg'),'practice_single_page')
	));
	/*  practice area single options */
	CSF::createSection($prefix.'_theme_options',array(
		'id' => 'case_single_page',
		'title' => esc_html__('Case Study Single Page','attorg'),
		'parent' => 'pages_and_template',
		'icon' => 'fa fa-calendar-check-o',
		'fields' => Attorg_Group_Fields::page_layout_options(esc_html__('Case Study Single Area','attorg'),'case_single_page')
	));
	/*  practice area single options */
	CSF::createSection($prefix.'_theme_options',array(
		'id' => 'attorney_page',
		'title' => esc_html__('Attorney Page','attorg'),
		'parent' => 'pages_and_template',
		'icon' => 'fa fa-user',
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' => '<h3>'.esc_html__('Attorney Single Page Options','attorg').'</h3>',
			),
			array(
				'id' => 'attorney_single_bg_color',
				'type' => 'color',
				'title' => esc_html__('Page Background Color','attorg'),
				'default' => '#ffffff'
			),
			array(
				'id' => 'attorney_single_spacing_top',
				'title' => esc_html__('Page Spacing Top','attorg'),
				'type' => 'slider',
				'desc' => wp_kses(__('you can set <mark>Padding Top</mark> for page content area.','attorg'),$allowed_html),
				'min'     => 0,
				'max'     => 500,
				'step'    => 1,
				'unit'    => 'px',
				'default' => 120,
			),
			array(
				'id' => 'attorney_single_spacing_bottom',
				'title' => esc_html__('Page Spacing Bottom','attorg'),
				'type' => 'slider',
				'desc' => wp_kses(__('you can set <mark>Padding Bottom</mark> for page content area.','attorg'),$allowed_html),
				'min'     => 0,
				'max'     => 500,
				'step'    => 1,
				'unit'    => 'px',
				'default' => 120,
			)
		)
	));

	/*-------------------------------------------------------
		   ** Typography  Options
	--------------------------------------------------------*/
	CSF::createSection($prefix.'_theme_options',array(
		'id' => 'typography',
		'title' => esc_html__('Typography','attorg'),
		'icon' => 'fa fa-text-width',
		'fields' => array(
			array(
				'type' => 'subheading',
				'content' => '<h3>'.esc_html__('Body Font Options','attorg').'</h3>',
			),
			array(
				'type' => 'typography',
				'title' => esc_html__('Typography','attorg'),
				'id' => '_body_font',
				'default' => array(
					'font-family' => 'Poppins',
					'font-size'   => '16',
					'line-height' => '26',
					'unit'        => 'px',
					'type'        => 'google',
				),
				'color' => false,
				'subset' => false,
				'text_align' => false,
				'text_transform' => false,
				'letter_spacing' => false,
				'desc' => wp_kses(__('you can set <mark>font</mark> for all html tags (if not use different heading font)','attorg'),$allowed_html),
			),
			array(
				'id'          => 'body_font_variant',
				'type'        => 'select',
				'title'       => esc_html__('Load Font Variant','attorg'),
				'multiple'    => true,
				'chosen'      => true,
				'options' => array(
					'300' => esc_html__('Light 300','attorg'),
					'400' => esc_html__('Regular 400','attorg'),
					'500' => esc_html__('Medium 500','attorg'),
					'600' => esc_html__('Semi Bold 600','attorg'),
					'700' => esc_html__('Bold 700','attorg'),
					'800' => esc_html__('Extra Bold 800','attorg'),
				),
				'default'     => array('400','700')
			),
			array(
				'type' => 'subheading',
				'content' => '<h3>'.esc_html__('Heading Font Options','attorg').'</h3>',
			),
			array(
				'type' => 'switcher',
				'id' => 'heading_font_enable',
				'title' => esc_html__('Heading Font','attorg'),
				'desc' => wp_kses(__('you can set <mark>yes</mark> to select different heading font','attorg'),$allowed_html),
				'default' => true
			),
			array(
				'type' => 'typography',
				'title' => esc_html__('Typography','attorg'),
				'id' => 'heading_font',
				'default' => array(
					'font-family' => 'Source Serif Pro',
					'type'        => 'google',
				),
				'color' => false,
				'subset' => false,
				'text_align' => false,
				'text_transform' => false,
				'letter_spacing' => false,
				'font_size' => false,
				'line_height' => false,
				'desc' => wp_kses(__('you can set <mark>font</mark> for  for heading tag .eg: h1,h2mh3,h4,h5,h6','attorg'),$allowed_html),
				'dependency' => array('heading_font_enable' ,'==','true')
			),
			array(
				'id'          => 'heading_font_variant',
				'type'        => 'select',
				'title'       => esc_html__('Load Font Variant','attorg'),
				'multiple'    => true,
				'chosen'      => true,
				'options' => array(
					'300' => esc_html__('Light 300','attorg'),
					'400' => esc_html__('Regular 400','attorg'),
					'500' => esc_html__('Medium 500','attorg'),
					'600' => esc_html__('Semi Bold 600','attorg'),
					'700' => esc_html__('Bold 700','attorg'),
					'800' => esc_html__('Extra Bold 800','attorg'),
				),
				'default'     => array('400','500','600','700','800'),
				'dependency' => array('heading_font_enable' ,'==','true')
			),
		)
	));

	/*-------------------------------------------------------
		   ** Backup  Options
	--------------------------------------------------------*/
	CSF::createSection($prefix.'_theme_options',array(
		'id' => 'backup',
		'title' => esc_html__('Import / Export','attorg'),
		'icon' => 'fa fa-upload',
		'fields' => array(
			array(
				'type' => 'notice',
				'style' => 'warning',
				'content' => esc_html__('You can save your current options. Download a Backup and Import.','attorg'),
			),
			array(
				'type' => 'backup',
				'title' => esc_html__('Backup & Import','attorg')
			)
		)
	));
}
