<?php
/**
 * theme shortcode generator
 * @since 1.0.0
 * */
if (!defined('ABSPATH')){
	exit(); //exit if access it directly
}

// Control core classes for avoid errors
if( class_exists( 'CSF' ) ) {
	$prefix = 'attorg';
	CSF::createShortcoder( $prefix.'_shortcodes', array(
		'button_title'   => esc_html__('Add Shortcode','attorg'),
		'select_title'   => esc_html__('Select a shortcode','attorg'),
		'insert_title'   => esc_html__('Insert Shortcode','attorg')
	) );

	/*------------------------------------
		Inline info link options
	-------------------------------------*/
	CSF::createSection( $prefix.'_shortcodes', array(
		'title'     => esc_html__('Inline Info Link','attorg'),
		'view'      => 'group',
		'shortcode' => 'attorg_info_item_wrap',
		'group_shortcode' => 'attorg_info_link',
		'group_fields'    => array(
			array(
				'id'    => 'icon',
				'type'  => 'icon',
				'title' => esc_html__('Icon','attorg'),
			),
			array(
				'id'      => 'text',
				'type'    => 'text',
				'title'   => esc_html__('Text','attorg'),
			),
			array(
				'id'      => 'url',
				'type'    => 'text',
				'title'   => esc_html__('URL','attorg'),
			)
		)
	) );
	/*------------------------------------
		info item two
	-------------------------------------*/
	CSF::createSection( $prefix.'_shortcodes', array(
		'title'     => esc_html__('Info Item Two','attorg'),
		'view'      => 'group',
		'shortcode' => 'attorg_info_item_two_wrap',
		'group_shortcode' => 'attorg_info_item_two',
		'group_fields'    => array(
			array(
				'id'    => 'icon',
				'type'  => 'icon',
				'title' => esc_html__('Icon','attorg'),
			),
			array(
				'id'      => 'title',
				'type'    => 'text',
				'title'   => esc_html__('Title','attorg'),
			),
			array(
				'id'      => 'link',
				'type'    => 'text',
				'title'   => esc_html__('Link','attorg'),
			)
		)
	) );

	/*------------------------------------
		info item Text
	-------------------------------------*/
	CSF::createSection( $prefix.'_shortcodes', array(
		'title'     => esc_html__('Info Item Text','attorg'),
		'view'      => 'group',
		'shortcode' => 'attorg_info_text_wrap',
		'group_shortcode' => 'attorg_info_text_item',
		'group_fields'    => array(
			array(
				'id'    => 'icon',
				'type'  => 'icon',
				'title' => esc_html__('Icon','attorg'),
			),
			array(
				'id'      => 'text',
				'type'    => 'text',
				'title'   => esc_html__('Text','attorg'),
			)
		)
	) );

}
