<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package attorg
 */

get_header();

?>
    <div id="primary" class="content-area attorney-single-page padding-120">
        <main id="main" class="site-main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
						<?php
						while ( have_posts() ) :
							the_post();
							get_template_part( 'template-parts/content', 'attorney-single' );
						endwhile; // End of the loop.
						?>
                    </div>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->
<?php
get_footer();
