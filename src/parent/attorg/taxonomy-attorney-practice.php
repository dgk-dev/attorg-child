<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package attorg
 */

get_header();
?>

    <div id="primary" class="content-area attorney-practice-taxonomy-content-area padding-120">
        <main id="main" class="site-main">
            <div class="container">
                <div class="row">
						<?php if ( have_posts() ) : ?>

							<?php
							/* Start the Loop */
							while ( have_posts() ) :
								the_post();

								/*
								 * Include the Post-Type-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
								 */
								get_template_part( 'template-parts/content-attorney-practice-area' );

							endwhile;
							?>
                            <div class="blog-pagination text-center">
								<?php Attorg()->post_pagination();?>
                            </div>

						<?php
						else :

							get_template_part( 'template-parts/content', 'none' );

						endif;
						?>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php

get_footer();
