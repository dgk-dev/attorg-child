<?php
$attorg = Attorg();
$post_meta = Attorg_Group_Fields_Value::post_meta('blog_post');
?>
<ul class="post-meta">
    <?php if ($post_meta['posted_by']):?>
	<li><?php $attorg->posted_on();?></li>
    <?php endif;?>
    <?php if ($post_meta['posted_on']):?>
	<li><?php $attorg->posted_by();?></li>
    <?php endif;?>
	<?php if ($post_meta['posted_cat']):?>
        <li>
            <div class="cats"><i class="fa fa-tags"></i> <?php the_category(', ');?></div>
        </li>
	<?php endif;?>
</ul>