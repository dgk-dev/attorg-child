<?php
$img_id = get_post_thumbnail_id(get_the_ID()) ? get_post_thumbnail_id(get_the_ID()) : false ;
$img_url_val = $img_id ? wp_get_attachment_image_src($img_id,'full',false) : '';
$img_url = is_array($img_url_val) && !empty($img_url_val) ? $img_url_val[0] : '';
$attorney_meta = get_post_meta(get_the_ID(),'attorg_attorney_options',true);
$designation = isset($attorney_meta['designation']) && !empty($attorney_meta['designation']) ? $attorney_meta['designation'] : '';
?>
<div class="col-lg-6">
	<div class="single-our-attoryney-item margin-bottom-30">
		<div class="img-wrapper">
			<div class="bg-image" style="background-image: url(<?php echo esc_url($img_url);?>)"></div>
		</div>
		<div class="content">
			<h4 class="title"><a href="<?php the_permalink();?>"><?php the_title()?></a></h4>
			<span class="designation"><?php echo esc_html($designation);?></span>
			<span class="separator"></span>
			<p><?php Attorg_excerpt(25);?> </p>
		</div>
	</div>
</div>