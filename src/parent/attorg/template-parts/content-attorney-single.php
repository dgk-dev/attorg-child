<?php
/**
 * Template part for displaying single post
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package attorg
 */
$attorg = Attorg();
$attorney_meta = get_post_meta(get_the_ID(),'attorg_attorney_options',true);
$img_id = get_post_thumbnail_id(get_the_ID()) ? get_post_thumbnail_id(get_the_ID()) : false ;
$img_url_val = $img_id ? wp_get_attachment_image_src($img_id,'medium',false) : '';
$img_url = is_array($img_url_val) && !empty($img_url_val) ? $img_url_val[0] : '';
$img_alt =  $img_id ? get_post_meta($img_id,'_wp_attachment_image_alt',true) : '';
$designation = isset($attorney_meta['designation']) && !empty($attorney_meta['designation']) ? $attorney_meta['designation'] : '';
$location = isset($attorney_meta['location']) && !empty($attorney_meta['location']) ? $attorney_meta['location'] : '';
$phone = isset($attorney_meta['phone']) && !empty($attorney_meta['phone']) ? $attorney_meta['phone'] : '';
$email = isset($attorney_meta['email']) && !empty($attorney_meta['email']) ? $attorney_meta['email'] : '';
$languages = isset($attorney_meta['languages']) && !empty($attorney_meta['languages']) ? $attorney_meta['languages'] : '';
$social_icons = isset($attorney_meta['social-icons']) && !empty($attorney_meta['social-icons']) ? $attorney_meta['social-icons'] : '';
$contact_shortcode = isset($attorney_meta['contact_shortcode']) && !empty($attorney_meta['contact_shortcode']) ? $attorney_meta['contact_shortcode'] : '';
$practice_areas = wp_get_post_terms(get_the_ID(),'attorney-practice');
$practice_area_markup = '';
$a = 1;
foreach ($practice_areas as $area){
    $a++;
    $separator = ' , ';
    $separator = count($practice_areas) == $a ? $separator : '' ;
	$practice_area_markup .='<a href="'.get_term_link($area->term_id).'">'.$area->name.'</a>'.$separator;
}

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('attorney-details-content-area'); ?>>
    <div class="top-content-area">
        <div class="img-wrapper">
            <img src="<?php echo esc_url($img_url);?>" alt="<?php echo esc_attr($img_alt);?>">
        </div>
        <div class="content">
            <table class="attorney-post-meta">
                <tbody>
                <tr>
                    <td class="at-position"><?php esc_html_e('Position','attorg')?></td>
                    <td class="at-position-value">&nbsp; : <?php echo esc_html($designation);?></td>
                </tr>
                <tr class="at-practice">
                    <td><?php esc_html_e('Practice Areas','attorg');?></td>
                    <td>&nbsp; : <?php echo wp_kses_post($practice_area_markup);?></td>
                </tr>
                <tr class="at-location">
                    <td><?php esc_html_e('Location','attorg');?></td>
                    <td>&nbsp; : <?php echo esc_html($location);?></td>
                </tr>
                <tr class="at-phone">
                    <td><?php esc_html_e('Phone','attorg');?></td>
                    <td>&nbsp; : <?php echo esc_html($phone);?></td>
                </tr>
                <tr class="at-email">
                    <td><?php esc_html_e('Email','attorg');?></td>
                    <td>&nbsp; : <?php echo esc_html($email);?></td>
                </tr>
                <tr class="at-languages">
                    <td><?php esc_html_e('Languages','attorg');?></td>
                    <td>&nbsp; : <?php echo esc_html($languages);?></td>
                </tr>
                </tbody></table>
            <ul class="social-icon">
                <?php
                    if (!empty($social_icons)){
                        foreach ($social_icons as $item){
                            printf('<li><a href="%1$s"><i class="%2$s"></i></a></li>',$item['url'],$item['icon']);
                        }
                    }
                ?>
            </ul>
        </div>
    </div>
    <div class="bottom-content-area padding-top-120">
        <div class="row">
            <div class="col-lg-7">
                <div class="content-block">
                   <?php the_content();?>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="attorney-contact-form-wrap">
                    <h3 class="title"><?php esc_html_e('Contact With Me','attorg');?></h3>
                    <div class="attorney-contact-form">
                        <?php echo do_shortcode($contact_shortcode)?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->
