<div class="header-style-01">
	<div class="topbar-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="topbar-inner">
						<div class="left-contnet">
							<?php
                            $header_two_info_bar_shortcode = cs_get_option('header_two_left_side_content');
                                echo do_shortcode($header_two_info_bar_shortcode);
                            ?>
						</div>
						<div class="right-contnet">
                            <?php
                                $header_two_btn = cs_get_switcher_option('header_two_info_btn');
                                $header_two_info_btn_text = cs_get_option('header_two_info_btn_text');
                                $header_two_info_btn_link = cs_get_option('header_two_info_btn_link');
                                if ($header_two_btn){
                                    printf('<a class="boxed-btn" href="%1$s">%2$s</a>',esc_url($header_two_info_btn_link),esc_html($header_two_info_btn_text));
                                }
                            ?>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<nav class="navbar navbar-area navbar-expand-lg nav-style-01">
		<div class="container nav-container">
			<div class="responsive-mobile-menu">
				<div class="logo-wrapper">
					<?php
					$header_two_logo = cs_get_option('header_two_logo');
					if (has_custom_logo() && empty($header_two_logo['id'])){
						the_custom_logo();
					}elseif (!empty($header_two_logo['id'])){
						printf('<a class="site-logo" href="%1$s"><img src="%2$s" alt="%3$s"/></a>',get_home_url(),$header_two_logo['url'],$header_two_logo['alt']);
					}
					else{
						printf('<a class="site-title" href="%1$s">%2$s</a>',get_home_url(),get_bloginfo('title'));
					}
					?>
				</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#attorg_main_menu" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			</div>
				<?php
				wp_nav_menu(array(
					'theme_location' => 'main-menu',
					'menu_class' => 'navbar-nav',
					'container' => 'div',
					'container_class' => 'collapse navbar-collapse',
					'container_id' => 'attorg_main_menu'
				));
				?>
			<?php if (class_exists('WooCommerce')):?>
				<a class="attorg-header-cart" href="<?php echo wc_get_cart_url(); ?>"
				   title="<?php _e( 'View your shopping cart' ); ?>">
					<i class="fa fa-shopping-basket" aria-hidden="true"></i>
					<span class="cart-badge"><?php echo sprintf (  '%d', WC()->cart->get_cart_contents_count() ); ?></span>
				</a>
			<?php endif;?>
		</div>
	</nav>
</div>