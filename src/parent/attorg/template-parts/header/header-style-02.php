<div class="header-style-02">
	<nav class="navbar navbar-area navbar-expand-lg nav-style-02">
		<div class="container nav-container">
			<div class="responsive-mobile-menu">
				<div class="logo-wrapper">
					<?php
					$header_three_logo = cs_get_option('header_three_logo');
					if (has_custom_logo() && empty($header_three_logo['id'])){
						the_custom_logo();
					}elseif (!empty($header_three_logo['id'])){
						printf('<a class="site-logo" href="%1$s"><img src="%2$s" alt="%3$s"/></a>',get_home_url(),$header_three_logo['url'],$header_three_logo['alt']);
					}
					else{
						printf('<a class="site-title" href="%1$s">%2$s</a>',get_home_url(),get_bloginfo('title'));
					}
					?>
				</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#attorg_main_menu" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			</div>
				<?php
				wp_nav_menu(array(
					'theme_location' => 'main-menu',
					'menu_class' => 'navbar-nav',
					'container' => 'div',
					'container_class' => 'collapse navbar-collapse',
					'container_id' => 'attorg_main_menu'
				));
				?>
				<?php 
                $header_three_nav_btn = cs_get_switcher_option('header_three_nav_btn'); 
                $header_three_nav_btn_title = cs_get_option('header_three_nav_btn_title');
				$header_three_nav_btn_link = cs_get_option('header_sthreenav_btn_link');
                if ($header_three_nav_btn):
                ?>
            <div class="navbar-right-content">
                <a href="<?php echo esc_url($header_three_nav_btn_link);?>" class="nav_button"> <?php echo esc_html($header_three_nav_btn_title);?></a>
            </div>
            <?php  endif; ?> 
		</div>
	</nav>
</div> 