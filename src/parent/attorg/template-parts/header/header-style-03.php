<div class="topbar-area style-03">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="topbar-inner">
                    <?php
                        $header_four_info_bar_content = !empty(cs_get_option('header_four_info_bar_content')) ? cs_get_option('header_four_info_bar_content') : [];
                        foreach ($header_four_info_bar_content as $item){
                            printf('<div class="%1$s">',$item['header_four_info_bar_content_area']);
                            echo do_shortcode($item['header_four_info_bar_shortcodes']);
                            print '</div>';
                        }
                    ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="header-style-03">
	<nav class="navbar navbar-area navbar-expand-lg nav-style-02">
		<div class="container nav-container">
			<div class="responsive-mobile-menu">
				<div class="logo-wrapper">
                    <?php
                        $header_four_logo = cs_get_option('header_four_logo');
                        if (has_custom_logo() && empty($header_four_logo['id'])){
                            the_custom_logo();
                        }elseif (!empty($header_four_logo['id'])){
                            printf('<a class="site-logo" href="%1$s"><img src="%2$s" alt="%3$s"/></a>',get_home_url(),$header_four_logo['url'],$header_four_logo['alt']);
                        }
                        else{
                            printf('<a class="site-title" href="%1$s">%2$s</a>',get_home_url(),get_bloginfo('title'));
                        }
                    ?>
				</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#attorg_main_menu" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			</div>
				<?php
                    wp_nav_menu(array(
                        'theme_location' => 'main-menu',
                        'menu_class' => 'navbar-nav',
                        'container' => 'div',
                        'container_class' => 'collapse navbar-collapse',
                        'container_id' => 'attorg_main_menu'
                    ));
                ?>
                <?php 
                $header_four_nav_btn = cs_get_switcher_option('header_four_nav_btn'); 
                $header_four_nav_btn_title = cs_get_option('header_four_nav_btn_title');
				$header_four_nav_btn_link = cs_get_option('header_fournav_btn_link');
                if ($header_four_nav_btn):
                ?>
            <div class="navbar-right-content">
                <a href="<?php echo esc_url($header_four_nav_btn_link);?>" class="nav_button"> <?php echo esc_html($header_four_nav_btn_title);?></a>
            </div>
            <?php  endif; ?> 
		</div>
	</nav>
</div>