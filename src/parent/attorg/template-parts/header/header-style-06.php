<div class="header-style-06">
	<div class="topbar-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="topbar-inner">
						<div class="left-contnet"> 
							<?php
                            $header_seven_info_bar_shortcode = cs_get_option('header_seven_left_side_content');
                                echo do_shortcode($header_seven_info_bar_shortcode);   
                            ?>
						</div>
						<div class="right-contnet">
                            <?php
                                $header_seven_btn = cs_get_switcher_option('header_seven_info_btn'); 
                                $header_seven_info_btn_text = cs_get_option('header_seven_info_btn_text');
                                $header_seven_info_btn_link = cs_get_option('header_seven_info_btn_link');
                                if ($header_seven_btn){
                                    printf('<a class="boxed-btn" href="%1$s">%2$s <i class="fa fa-long-arrow-right"></i></a>',esc_url($header_seven_info_btn_link),esc_html($header_seven_info_btn_text));
                                }

                                $header_seven_info_bar_right_side_shortcode = cs_get_option('header_seven_right_side_content');
                                echo do_shortcode($header_seven_info_bar_right_side_shortcode);   
							?>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<nav class="navbar navbar-area navbar-expand-lg nav-style-01">
		<div class="container nav-container">
			<div class="responsive-mobile-menu">
				<div class="logo-wrapper">
					<?php
					$header_seven_logo = cs_get_option('header_seven_logo');
					if (has_custom_logo() && empty($header_seven_logo['id'])){
						the_custom_logo();
					}elseif (!empty($header_seven_logo['id'])){
						printf('<a class="site-logo" href="%1$s"><img src="%2$s" alt="%3$s"/></a>',get_home_url(),$header_seven_logo['url'],$header_seven_logo['alt']);
					}
					else{
						printf('<a class="site-title" href="%1$s">%2$s</a>',get_home_url(),get_bloginfo('title'));
					}
					?>
				</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#attorg_main_menu" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			</div>
				<?php
				wp_nav_menu(array(
					'theme_location' => 'main-menu',
					'menu_class' => 'navbar-nav',
					'container' => 'div',
					'container_class' => 'collapse navbar-collapse',
					'container_id' => 'attorg_main_menu'
				));
                ?>
                <?php 
                $header_seven_nav_btn = cs_get_switcher_option('header_seven_nav_btn'); 
                $header_seven_nav_btn_icon = cs_get_option('header_seven_nav_btn_icon');
                $header_seven_nav_btn_title = cs_get_option('header_seven_nav_btn_title');
				$header_seven_nav_btn_subtitle = cs_get_option('header_seven_nav_btn_subtitle');
				$header_seven_nav_btn_link = cs_get_option('header_seven_nav_btn_link');
                if ($header_seven_nav_btn):
                ?>
            <div class="navbar-right-content">
                <a href="<?php echo esc_url($header_seven_nav_btn_link);?>">
					<div class="nav-button-wrap">
						<div class="icon">
							<i class="<?php echo esc_attr($header_seven_nav_btn_icon);?>"></i>
						</div>
						<div class="content">
							<span class="subtitle"><?php echo esc_html($header_seven_nav_btn_subtitle);?></span>
							<h5 class="title"><?php echo esc_html($header_seven_nav_btn_title);?></h5>
						</div>
					</div>
				</a>
            </div>
            <?php  endif; ?>        
		</div>
	</nav>
</div>