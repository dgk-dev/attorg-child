<nav class="navbar navbar-area navbar-expand-lg nav-style-01 navbar-default">
	<div class="container nav-container">
		<div class="responsive-mobile-menu">
			<div class="logo-wrapper">
                <?php
                    $header_one_logo = cs_get_option('header_one_logo');
                    if (has_custom_logo() && empty($header_one_logo['id'])){
                        the_custom_logo();
                    }elseif (!empty($header_one_logo['id'])){
	                    printf('<a class="site-logo" href="%1$s"><img src="%2$s" alt="%3$s"/></a>',get_home_url(),$header_one_logo['url'],$header_one_logo['alt']);
                    }
                    else{
                        printf('<a class="site-title" href="%1$s">%2$s</a>',get_home_url(),get_bloginfo('title'));
                    }
                ?>
			</div>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#attorg_main_menu" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
		</div>
            <?php
                wp_nav_menu(array(
                    'theme_location' => 'main-menu',
                    'menu_class' => 'navbar-nav',
                    'container' => 'div',
                    'container_class' => 'collapse navbar-collapse',
                    'container_id' => 'attorg_main_menu'
                ));
            ?>
		<?php if (class_exists('WooCommerce')):?>
		<a class="attorg-header-cart" href="<?php echo wc_get_cart_url(); ?>"
		   title="<?php _e( 'View your shopping cart' ); ?>">
			<i class="fa fa-shopping-basket" aria-hidden="true"></i>
			<span class="cart-badge"><?php echo sprintf (  '%d', WC()->cart->get_cart_contents_count() ); ?></span>
		</a>
        <?php endif;?>
	</div>
</nav>