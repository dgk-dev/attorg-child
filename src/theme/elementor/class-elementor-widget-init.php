<?php
/**
 * All Elementor widget init
 * @package Attorg
 * @since 1.0.0
 */

if ( !defined('ABSPATH') ){
	exit(); // exit if access directly
}


if ( !class_exists('Wol_Elementor_Widget_Init') ){

	class Wol_Elementor_Widget_Init{
		/*
		* $instance
		* @since 1.0.0
		* */
		private static $instance;
		/*
		* construct()
		* @since 1.0.0
		* */
		public function __construct() {
			add_action( 'elementor/elements/categories_registered', array($this,'_widget_categories') );
			//elementor widget registered
			add_action('elementor/widgets/widgets_registered',array($this,'_widget_registered'));
			// elementor editor css
			//add_action( 'elementor/editor/after_enqueue_scripts', array($this,'load_assets_for_elementor'));
			//add_action( 'elementor/controls/controls_registered', array($this,'modify_controls'), 10, 1 );
		}
		/*
	   * getInstance()
	   * @since 1.0.0
	   * */
		public static function getInstance(){
			if ( null == self::$instance ){
				self::$instance = new self();
			}
			return self::$instance;
		}
		/**
		 * _widget_categories()
		 * @since 1.0.0
		 * */
		public function _widget_categories($elements_manager){
			$elements_manager->add_category(
				'wol_widgets',
				[
					'title' => __( 'Wol Widgets', 'attorg-master' ),
					'icon' => 'fa fa-plug',
				]
			);
		}

		/**
		 * _widget_registered()
		 * @since 1.0.0
		 * */
		public function _widget_registered(){
			if( !class_exists('Elementor\Widget_Base') ){
				return;
			}
			$elementor_widgets = array(
				'wol-features-box',
				'wol-header-one',
			);

			$elementor_widgets = apply_filters('wol_elementor_widget',$elementor_widgets);
			sort($elementor_widgets);
			if ( is_array($elementor_widgets) && !empty($elementor_widgets) ) {
				foreach ( $elementor_widgets as $widget ){
					if(file_exists(get_stylesheet_directory().'/elementor/widgets/class-'.$widget.'-elementor-widget.php')){
						require_once get_stylesheet_directory().'/elementor/widgets/class-'.$widget.'-elementor-widget.php';
					}
				}
			}
 
		}

		/**
		 * Adding custom icon to icon control in Elementor
		 */
		// public function modify_controls( $controls_registry ) {
		// 	// Get existing icons
		// 	$icons = $controls_registry->get_control( 'icon' )->get_settings( 'options' );

		// 	// Append new icons

		// 	$new_icons = array_merge(
		// 		array(
		// 			'flaticon-balance' => esc_html__('balance','attorg-master'),
		// 			'flaticon-auction' => esc_html__('auction','attorg-master'),
		// 			'flaticon-balance-1' => esc_html__('balance','attorg-master'),
		// 			'flaticon-courthouse' => esc_html__('courthouse','attorg-master'),
		// 			'flaticon-auction-1' => esc_html__('auction','attorg-master'),
		// 			'flaticon-balance-2' => esc_html__('balance','attorg-master'),
		// 			'flaticon-law' => esc_html__('law','attorg-master'),
		// 			'flaticon-employee' => esc_html__('employee','attorg-master'),
		// 			'flaticon-badge' => esc_html__('badge','attorg-master'),
		// 			'flaticon-jury' => esc_html__('jury','attorg-master'),
		// 			'flaticon-customer-review' => esc_html__('customer-review','attorg-master'),
		// 			'flaticon-award' => esc_html__('award','attorg-master'),
		// 			'flaticon-trophy' => esc_html__('trophy','attorg-master'),
		// 			'flaticon-quality' => esc_html__('quality','attorg-master'),
		// 			'flaticon-home' => esc_html__('home','attorg-master'),
		// 			'flaticon-call' => esc_html__('call','attorg-master'),
		// 			'flaticon-mail' => esc_html__('mail','attorg-master'),
		// 			'flaticon-email' => esc_html__('email','attorg-master'),
		// 			'flaticon-family' => esc_html__('family','attorg-master'),
		// 			'flaticon-family-1' => esc_html__('family','attorg-master'),
		// 			'flaticon-employee-1' => esc_html__('employee','attorg-master')
		// 		),
		// 		$icons
		// 	);

		// 	// Then we set a new list of icons as the options of the icon control
		// 	$controls_registry->get_control( 'icon' )->set_settings( 'options', $new_icons );
		// }

		// /**
		//  * load custom assets for elementor
		//  * @since 1.0.0
		//  * */
		// public function load_assets_for_elementor(){
		// 	wp_enqueue_style( 'flaticon',ATTORG_MASTER_CSS.'/flaticon.css');
		// }
	}

	if ( class_exists('Wol_Elementor_Widget_Init') ){
		Wol_Elementor_Widget_Init::getInstance();
	}

}//end if
