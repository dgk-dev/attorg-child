<?php
/**
 * Elementor Widget
 * @package Appside
 * @since 1.0.0
 */

namespace Elementor;
class Attorg_Wol_Features_Box extends Widget_Base {

    /**
     * Get widget name.
     *
     * Retrieve Elementor widget name.
     *
     * @return string Widget name.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_name() {
        return 'attorg-wol-features-box-widget';
    }

    /**
     * Get widget title.
     *
     * Retrieve Elementor widget title.
     *
     * @return string Widget title.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_title() {
        return esc_html__( 'WOL Features box', 'attorg-master' );
    }

    /**
     * Get widget icon.
     *
     * Retrieve Elementor widget icon.
     *
     * @return string Widget icon.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_icon() {
        return 'eicon-price-table';
    }

    /**
     * Get widget categories.
     *
     * Retrieve the list of categories the Elementor widget belongs to.
     *
     * @return array Widget categories.
     * @since 1.0.0
     * @access public
     *
     */
    public function get_categories() {
        return [ 'attorg_widgets' ];
    }

    /**
     * Register Elementor widget controls.
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since 1.0.0
     * @access protected
     */
    protected function _register_controls() {
        $this->start_controls_section(
            'settings_section',
            [
                'label' => esc_html__( 'General Settings', 'attorg-master' ),
                'tab'   => Controls_Manager::TAB_CONTENT,
            ]
        );
        $this->add_control('title',[
            'label' => esc_html__('Title','attorg-master'),
            'type' =>  Controls_Manager::TEXT,
            'default' => esc_html__('Personal','attorg-master'),
            'description' => esc_html__('enter price plan title','attorg-master')
        ]);
        $this->add_control('img',[
            'label' => esc_html__('Image','attorg-master'),
            'type' =>  Controls_Manager::MEDIA,
            'description' => esc_html__('select image','attorg-master')
        ]);
        $this->add_control('btn_label',[
            'label' => esc_html__('Button label','attorg-master'),
            'type' =>  Controls_Manager::TEXT,
            'default' => esc_html__('Order Now','attorg-master'),
            'description' => esc_html__('enter button label','attorg-master')
        ]);
        $this->add_control('btn_url',[
            'label' => esc_html__('Button URL','attorg-master'),
            'type' =>  Controls_Manager::URL,
            'default' => [
                   'url' =>  '#'
            ],
            'description' => esc_html__('enter button label','attorg-master')
        ]);
        $this->add_control( 'feature_list_items', [
            'label'       => esc_html__( 'Info list Item', 'attorg-master' ),
            'type'        => Controls_Manager::REPEATER,
            'default'     => [
                [
                    'feature' => esc_html__('Feature','attorg-master'),
                    'bullet' => esc_html__('Bullet','attorg-master'),
                ]
            ],
            'fields'      => [
                [
                    'name'        => 'feature',
                    'label'       => esc_html__( 'Feature', 'attorg-master' ),
                    'type'        => Controls_Manager::TEXTAREA,
                    'default' => esc_html__('Feature','attrog-master'),
                    'description' => esc_html__( 'Feature text' , 'attorg-master' )
                ],
                [
                    'name'        => 'bullet',
                    'label'       => esc_html__( 'Bullet', 'attorg-master' ),
                    'type'        => Controls_Manager::ICON,
                    'default' => 'fa fa-check',
                    'description' => esc_html__( 'Feature bullet', 'attorg-master' )
                ],
            ],
        ] );
        $this->end_controls_section();

    }

    /**
     * Render Elementor widget output on the frontend.
     *
     * Written in PHP and used to generate the final HTML.
     *
     * @since 1.0.0
     * @access protected
     */
    protected function render() {
        $settings = $this->get_settings_for_display();
        $feature_list_items = $settings['feature_list_items'];
        ?>
        <div class="single-price-plan-01 wol-features-box">
            <div class="price-header">
                <h3 class="title"><?php echo esc_html($settings['title'])?></h3>

                <img src="<?php echo esc_html($settings['img']['url']); ?>" alt="<?php echo get_post_meta($settings['img']['id'], '_wp_attachment_image_alt', TRUE); ?>">

            </div>
            <div class="features-body">
                <ul class="fa-ul">
                    <?php foreach ( $feature_list_items as $feature_list_item ): ?>
                        <li><i class="fa-li <?php echo esc_html($feature_list_item['bullet'])?>"></i><?php echo esc_html($feature_list_item['feature'])?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="price-footer">
                <a class="order-btn" href="<?php echo esc_url($settings['btn_url']['url'])?>"><?php echo esc_html($settings['btn_label'])?></a>
            </div>
        </div>
        <?php
    }
}

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Attorg_Wol_Features_Box() );