<?php
/**
 * Elementor Widget
 * @package Attorg
 * @since 1.0.0
 */

namespace Elementor;
class Attorg_Wol_Header_Area_One_Widget extends Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Elementor widget name.
	 *
	 * @return string Widget name.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_name() {
		return 'wol-header-area-one-widget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Elementor widget title.
	 *
	 * @return string Widget title.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_title() {
		return esc_html__( 'WOL Header Slider One', 'attorg-master' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Elementor widget icon.
	 *
	 * @return string Widget icon.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_icon() {
		return 'eicon-archive-title';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Elementor widget belongs to.
	 *
	 * @return array Widget categories.
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function get_categories() {
		return [ 'wol_widgets' ];
	}

	/**
	 * Register Elementor widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'settings_section',
			[
				'label' => esc_html__( 'Section Contents', 'attorg-master' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);
		$this->add_control('slider_alignment',[
			'label' => esc_html('Alignment','attorg-master'),
			'type' => Controls_Manager::CHOOSE,
			'options' => [
				'left' => [
					'title' => __( 'Left', 'plugin-domain' ),
					'icon' => 'fa fa-align-left',
				],
				'center' => [
					'title' => __( 'Center', 'plugin-domain' ),
					'icon' => 'fa fa-align-center',
				],
				'right' => [
					'title' => __( 'Right', 'plugin-domain' ),
					'icon' => 'fa fa-align-right',
				],
			],
			'default' => 'left',
			'description' => esc_html('choose alignment for slider text','attorg-master')
		]);
		$this->add_control( 'header_sliders',
			[
				'title'       => esc_html__( 'Header Slider Items', 'attorg-master' ),
				'type'        => Controls_Manager::REPEATER,
				'default'     => [
					[
						'title'        => esc_html__( 'Dont Feel Helpless We Fight for Justice', 'attorg-master' ),
						'subtitle'     => esc_html__( 'WE fight for justice', 'attorg-master' ),
						'description'  => esc_html__( 'Bore tall nay many many time yet less. Doubtful for answered one fat indulged margaret sir shutters together. Ladies so in wholly around whence in at. Warmth he up giving oppose if.', 'attorg-master' ),
						'btn_one_text' => esc_html__( 'Practive Area', 'attorg-master' ),
						'btn_one_url'  => esc_url( '#' ),
						'btn_two_text' => esc_html__( 'Contact Us', 'attorg-master' ),
						'btn_two_url'  => esc_url( '#' ),
					],
					[
						'title'        => esc_html__( 'Dont Feel Helpless We Fight for Justice', 'attorg-master' ),
						'subtitle'     => esc_html__( 'WE fight for justice', 'attorg-master' ),
						'description'  => esc_html__( 'Bore tall nay many many time yet less. Doubtful for answered one fat indulged margaret sir shutters together. Ladies so in wholly around whence in at. Warmth he up giving oppose if.', 'attorg-master' ),
						'btn_one_text' => esc_html__( 'Practive Area', 'attorg-master' ),
						'btn_one_url'  => esc_url( '#' ),
						'btn_two_text' => esc_html__( 'Contact Us', 'attorg-master' ),
						'btn_two_url'  => esc_url( '#' ),
					],
				],
				'fields'      => [
					[
						'name'        => 'title',
						'label'       => esc_html__( 'Title', 'attorg-master' ),
						'type'        => Controls_Manager::TEXT,
						'default'     => esc_html__( 'Dont Feel Helpless We Fight for Justice', 'attorg-master' ),
						'description' => esc_html__( 'enter title', 'attorg-master' )
					],
					[
						'name'        => 'subtitle_status',
						'label'       => esc_html__( 'Subtitle Show/Hide', 'attorg-master' ),
						'type'        => Controls_Manager::SWITCHER,
						'default'     => 'yes',
						'description' => esc_html__( 'enter subtitle', 'attorg-master' ),
						''
					],
					[
						'name'        => 'subtitle',
						'label'       => esc_html__( 'Subtitle', 'attorg-master' ),
						'type'        => Controls_Manager::TEXT,
						'default'     => esc_html__( 'WE fight for justice', 'attorg-master' ),
						'description' => esc_html__( 'enter subtitle', 'attorg-master' ),
						'condition'   => [
							'subtitle_status' => 'yes'
						]
					],
					[
						'name'        => 'description_status',
						'label'       => esc_html__( 'Description Show/Hide', 'attorg-master' ),
						'type'        => Controls_Manager::SWITCHER,
						'default'     => 'yes',
						'description' => esc_html__( 'enter description', 'attorg-master' ),
					],
					[
						'name'        => 'description',
						'label'       => esc_html__( 'Description', 'attorg-master' ),
						'type'        => Controls_Manager::TEXTAREA,
						'default'     => esc_html__( 'Bore tall nay many many time yet less. Doubtful for answered one fat indulged margaret sir shutters together. Ladies so in wholly around whence in at. Warmth he up giving oppose if.', 'attorg-master' ),
						'description' => esc_html__( 'enter description', 'attorg-master' ),
						'condition'   => [
							'description_status' => 'yes'
						]
					],
					[
						'name'        => 'btn_one_status',
						'label'       => esc_html__( 'Button One Show/Hide', 'attorg-master' ),
						'type'        => Controls_Manager::SWITCHER,
						'default'     => 'yes',
						'description' => esc_html__( 'show/hide button one', 'attorg-master' ),
						'condition'   => [
							'description_status' => 'yes'
						]
					],
					[
						'name'        => 'btn_one_text',
						'label'       => esc_html__( 'Button One Label', 'attorg-master' ),
						'type'        => Controls_Manager::TEXT,
						'default'     => esc_html__( 'Practice Area', 'attorg-master' ),
						'description' => esc_html__( 'enter button one text', 'attorg-master' ),
						'condition'   => [
							'btn_one_status' => 'yes'
						]
					],
					[
						'name'        => 'btn_one_url',
						'label'       => esc_html__( 'Button One URL', 'attorg-master' ),
						'type'        => Controls_Manager::URL,
						'default'     => [
							'url' => '#'
						],
						'description' => esc_html__( 'enter button one url', 'attorg-master' ),
						'condition'   => [
							'btn_one_status' => 'yes'
						]
					],
					[
						'name'        => 'btn_two_status',
						'label'       => esc_html__( 'Button Two Show/Hide', 'attorg-master' ),
						'type'        => Controls_Manager::SWITCHER,
						'default'     => 'yes',
						'description' => esc_html__( 'show/hide button two', 'attorg-master' )
					],
					[
						'name'        => 'btn_two_text',
						'label'       => esc_html__( 'Button Two Label', 'attorg-master' ),
						'type'        => Controls_Manager::TEXT,
						'default'     => esc_html__( 'Contact Us', 'attorg-master' ),
						'description' => esc_html__( 'enter button two text', 'attorg-master' ),
						'condition'   => [
							'btn_two_status' => 'yes'
						]
					],
					[
						'name'        => 'btn_two_url',
						'label'       => esc_html__( 'Button Two URL', 'attorg-master' ),
						'type'        => Controls_Manager::URL,
						'default'     => [
							'url' => '#'
						],
						'description' => esc_html__( 'enter button two url', 'attorg-master' ),
						'condition'   => [
							'btn_two_status' => 'yes'
						]
					],
					[
                        'type'     => Controls_Manager::MEDIA,
                        'name'     => 'background_image_desktop',
                        'default' => [
                                'src' => Utils::get_placeholder_image_src()
                        ],
                        'label'    => esc_html__( 'Background Image Desktop', 'attorg-master' ),
                        'description' => esc_html__('upload background image for desktop','attorg-master')
					],
					[
                        'type'     => Controls_Manager::MEDIA,
                        'name'     => 'background_image_mobile',
                        'default' => [
                                'src' => Utils::get_placeholder_image_src()
                        ],
                        'label'    => esc_html__( 'Background Image Mobile', 'attorg-master' ),
                        'description' => esc_html__('upload background image for mobile','attorg-master')
					]
				],
				'title_field' => "{{title}}"
			]
		);
		$this->end_controls_section();
		$this->start_controls_section(
			'slider_settings_section',
			[
				'label' => esc_html__( 'Slider Settings', 'appside-master' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);
		$this->add_control(
			'items',
			[
				'label'       => esc_html__( 'Items', 'appside-master' ),
				'type'        => Controls_Manager::TEXT,
				'description' => esc_html__( 'you can set how many item show in slider', 'appside-master' ),
				'default'     => '1'
			]
		);
		$this->add_control(
			'margin',
			[
				'label'       => esc_html__( 'Margin', 'appside-master' ),
				'description' => esc_html__( 'you can set margin for slider', 'appside-master' ),
				'type'        => Controls_Manager::SLIDER,
				'range'       => [
					'px' => [
						'min'  => 0,
						'max'  => 100,
						'step' => 5,
					]
				],
				'default'     => [
					'unit' => 'px',
					'size' => 0,
				],
				'size_units'  => [ 'px' ],
				'condition'   => array(
					'autoplay' => 'yes'
				)
			]
		);
		$this->add_control(
			'loop',
			[
				'label'       => esc_html__( 'Loop', 'appside-master' ),
				'type'        => Controls_Manager::SWITCHER,
				'description' => esc_html__( 'you can set yes/no to enable/disable', 'appside-master' ),
				'default'     => 'yes'
			]
		);
		$this->add_control(
			'nav',
			[
				'label'       => esc_html__( 'Nav', 'appside-master' ),
				'type'        => Controls_Manager::SWITCHER,
				'description' => esc_html__( 'you can set yes/no to enable/disable', 'appside-master' ),
				'default'     => 'yes'
			]
		);
		$this->add_control(
			'autoplay',
			[
				'label'       => esc_html__( 'Autoplay', 'appside-master' ),
				'type'        => Controls_Manager::SWITCHER,
				'description' => esc_html__( 'you can set yes/no to enable/disable', 'appside-master' ),
				'default'     => 'yes'
			]
		);
		$this->add_control(
			'autoplaytimeout',
			[
				'label'      => esc_html__( 'Autoplay Timeout', 'appside-master' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min'  => 0,
						'max'  => 10000,
						'step' => 2,
					]
				],
				'default'    => [
					'unit' => 'px',
					'size' => 5000,
				],
				'size_units' => [ 'px' ],
				'condition'  => array(
					'autoplay' => 'yes'
				)
			]
		);
		$this->add_control(
			'animate_in',
			[
				'label'       => esc_html__( 'Animate In', 'appside-master' ),
				'type'        => Controls_Manager::SELECT,
				'options' => [
					'bounce'             => esc_html__( 'bounce', 'xg-product' ),
					'flash'              => esc_html__( 'flash', 'xg-product' ),
					'pulse'              => esc_html__( 'pulse', 'xg-product' ),
					'rubberBand'         => esc_html__( 'rubberBand', 'xg-product' ),
					'shake'              => esc_html__( 'shake', 'xg-product' ),
					'swing'              => esc_html__( 'swing', 'xg-product' ),
					'tada'               => esc_html__( 'tada', 'xg-product' ),
					'wobble'             => esc_html__( 'wobble', 'xg-product' ),
					'jello'              => esc_html__( 'jello', 'xg-product' ),
					'bounceIn'           => esc_html__( 'bounceIn', 'xg-product' ),
					'bounceInDown'       => esc_html__( 'bounceInDown', 'xg-product' ),
					'bounceInLeft'       => esc_html__( 'bounceInLeft', 'xg-product' ),
					'bounceInRight'      => esc_html__( 'bounceInRight', 'xg-product' ),
					'bounceInUp'         => esc_html__( 'bounceInUp', 'xg-product' ),
					'bounceOut'          => esc_html__( 'bounceOut', 'xg-product' ),
					'bounceOutDown'      => esc_html__( 'bounceOutDown', 'xg-product' ),
					'bounceOutLeft'      => esc_html__( 'bounceOutLeft', 'xg-product' ),
					'bounceOutRight'     => esc_html__( 'bounceOutRight', 'xg-product' ),
					'bounceOutUp'        => esc_html__( 'bounceOutUp', 'xg-product' ),
					'fadeIn'             => esc_html__( 'fadeIn', 'xg-product' ),
					'fadeInDown'         => esc_html__( 'fadeInDown', 'xg-product' ),
					'fadeInDownBig'      => esc_html__( 'fadeInDownBig', 'xg-product' ),
					'fadeInLeft'         => esc_html__( 'fadeInLeft', 'xg-product' ),
					'fadeInLeftBig'      => esc_html__( 'fadeInLeftBig', 'xg-product' ),
					'fadeInRight'        => esc_html__( 'fadeInRight', 'xg-product' ),
					'fadeInRightBig'     => esc_html__( 'fadeInRightBig', 'xg-product' ),
					'fadeInUp'           => esc_html__( 'fadeInUp', 'xg-product' ),
					'fadeInUpBig'        => esc_html__( 'fadeInUpBig', 'xg-product' ),
					'fadeOut'            => esc_html__( 'fadeOut', 'xg-product' ),
					'fadeOutDown'        => esc_html__( 'fadeOutDown', 'xg-product' ),
					'fadeOutDownBig'     => esc_html__( 'fadeOutDownBig', 'xg-product' ),
					'fadeOutLeft'        => esc_html__( 'fadeOutLeft', 'xg-product' ),
					'fadeOutLeftBig'     => esc_html__( 'fadeOutLeftBig', 'xg-product' ),
					'fadeOutRight'       => esc_html__( 'fadeOutRight', 'xg-product' ),
					'fadeOutRightBig'    => esc_html__( 'fadeOutRightBig', 'xg-product' ),
					'fadeOutUp'          => esc_html__( 'fadeOutUp', 'xg-product' ),
					'fadeOutUpBig'       => esc_html__( 'fadeOutUpBig', 'xg-product' ),
					'flip'               => esc_html__( 'flip', 'xg-product' ),
					'flipInX'            => esc_html__( 'flipInX', 'xg-product' ),
					'flipInY'            => esc_html__( 'flipInY', 'xg-product' ),
					'flipOutX'           => esc_html__( 'flipOutX', 'xg-product' ),
					'flipOutY'           => esc_html__( 'flipOutY', 'xg-product' ),
					'lightSpeedIn'       => esc_html__( 'lightSpeedIn', 'xg-product' ),
					'lightSpeedOut'      => esc_html__( 'lightSpeedOut', 'xg-product' ),
					'rotateIn'           => esc_html__( 'rotateIn', 'xg-product' ),
					'rotateInDownLeft'   => esc_html__( 'rotateInDownLeft', 'xg-product' ),
					'rotateInDownRight'  => esc_html__( 'rotateInDownRight', 'xg-product' ),
					'rotateInUpLeft'     => esc_html__( 'rotateInUpLeft', 'xg-product' ),
					'rotateInUpRight'    => esc_html__( 'rotateInUpRight', 'xg-product' ),
					'rotateOut'          => esc_html__( 'rotateOut', 'xg-product' ),
					'rotateOutDownLeft'  => esc_html__( 'rotateOutDownLeft', 'xg-product' ),
					'rotateOutDownRight' => esc_html__( 'rotateOutDownRight', 'xg-product' ),
					'rotateOutUpLeft'    => esc_html__( 'rotateOutUpLeft', 'xg-product' ),
					'rotateOutUpRight'   => esc_html__( 'rotateOutUpRight', 'xg-product' ),
					'slideInUp'          => esc_html__( 'slideInUp', 'xg-product' ),
					'slideInDown'        => esc_html__( 'slideInDown', 'xg-product' ),
					'slideInLeft'        => esc_html__( 'slideInLeft', 'xg-product' ),
					'slideInRight'       => esc_html__( 'slideInRight', 'xg-product' ),
					'slideOutUp'         => esc_html__( 'slideOutUp', 'xg-product' ),
					'slideOutDown'       => esc_html__( 'slideOutDown', 'xg-product' ),
					'slideOutLeft'       => esc_html__( 'slideOutLeft', 'xg-product' ),
					'slideOutRight'      => esc_html__( 'slideOutRight', 'xg-product' ),
					'zoomIn'             => esc_html__( 'zoomIn', 'xg-product' ),
					'zoomInDown'         => esc_html__( 'zoomInDown', 'xg-product' ),
					'zoomInLeft'         => esc_html__( 'zoomInLeft', 'xg-product' ),
					'zoomInRight'        => esc_html__( 'zoomInRight', 'xg-product' ),
					'zoomInUp'           => esc_html__( 'zoomInUp', 'xg-product' ),
					'zoomOut'            => esc_html__( 'zoomOut', 'xg-product' ),
					'zoomOutDown'        => esc_html__( 'zoomOutDown', 'xg-product' ),
					'zoomOutLeft'        => esc_html__( 'zoomOutLeft', 'xg-product' ),
					'zoomOutRight'       => esc_html__( 'zoomOutRight', 'xg-product' ),
					'zoomOutUp'          => esc_html__( 'zoomOutUp', 'xg-product' ),
					'hinge'              => esc_html__( 'hinge', 'xg-product' ),
					'jackInTheBox'       => esc_html__( 'jackInTheBox', 'xg-product' ),
					'rollIn'             => esc_html__( 'rollIn', 'xg-product' ),
					'rollOut'            => esc_html__( 'rollOut', 'xg-product' ),
				],
				'default' => 'fadeIn',
				'description' => esc_html__( 'you can set animation in for slider', 'appside-master' ),
			]
		);
		$this->add_control(
			'animate_out',
			[
				'label'       => esc_html__( 'Animate Out', 'appside-master' ),
				'type'        => Controls_Manager::SELECT,
				'options' => [
					'bounce'             => esc_html__( 'bounce', 'xg-product' ),
					'flash'              => esc_html__( 'flash', 'xg-product' ),
					'pulse'              => esc_html__( 'pulse', 'xg-product' ),
					'rubberBand'         => esc_html__( 'rubberBand', 'xg-product' ),
					'shake'              => esc_html__( 'shake', 'xg-product' ),
					'swing'              => esc_html__( 'swing', 'xg-product' ),
					'tada'               => esc_html__( 'tada', 'xg-product' ),
					'wobble'             => esc_html__( 'wobble', 'xg-product' ),
					'jello'              => esc_html__( 'jello', 'xg-product' ),
					'bounceIn'           => esc_html__( 'bounceIn', 'xg-product' ),
					'bounceInDown'       => esc_html__( 'bounceInDown', 'xg-product' ),
					'bounceInLeft'       => esc_html__( 'bounceInLeft', 'xg-product' ),
					'bounceInRight'      => esc_html__( 'bounceInRight', 'xg-product' ),
					'bounceInUp'         => esc_html__( 'bounceInUp', 'xg-product' ),
					'bounceOut'          => esc_html__( 'bounceOut', 'xg-product' ),
					'bounceOutDown'      => esc_html__( 'bounceOutDown', 'xg-product' ),
					'bounceOutLeft'      => esc_html__( 'bounceOutLeft', 'xg-product' ),
					'bounceOutRight'     => esc_html__( 'bounceOutRight', 'xg-product' ),
					'bounceOutUp'        => esc_html__( 'bounceOutUp', 'xg-product' ),
					'fadeIn'             => esc_html__( 'fadeIn', 'xg-product' ),
					'fadeInDown'         => esc_html__( 'fadeInDown', 'xg-product' ),
					'fadeInDownBig'      => esc_html__( 'fadeInDownBig', 'xg-product' ),
					'fadeInLeft'         => esc_html__( 'fadeInLeft', 'xg-product' ),
					'fadeInLeftBig'      => esc_html__( 'fadeInLeftBig', 'xg-product' ),
					'fadeInRight'        => esc_html__( 'fadeInRight', 'xg-product' ),
					'fadeInRightBig'     => esc_html__( 'fadeInRightBig', 'xg-product' ),
					'fadeInUp'           => esc_html__( 'fadeInUp', 'xg-product' ),
					'fadeInUpBig'        => esc_html__( 'fadeInUpBig', 'xg-product' ),
					'fadeOut'            => esc_html__( 'fadeOut', 'xg-product' ),
					'fadeOutDown'        => esc_html__( 'fadeOutDown', 'xg-product' ),
					'fadeOutDownBig'     => esc_html__( 'fadeOutDownBig', 'xg-product' ),
					'fadeOutLeft'        => esc_html__( 'fadeOutLeft', 'xg-product' ),
					'fadeOutLeftBig'     => esc_html__( 'fadeOutLeftBig', 'xg-product' ),
					'fadeOutRight'       => esc_html__( 'fadeOutRight', 'xg-product' ),
					'fadeOutRightBig'    => esc_html__( 'fadeOutRightBig', 'xg-product' ),
					'fadeOutUp'          => esc_html__( 'fadeOutUp', 'xg-product' ),
					'fadeOutUpBig'       => esc_html__( 'fadeOutUpBig', 'xg-product' ),
					'flip'               => esc_html__( 'flip', 'xg-product' ),
					'flipInX'            => esc_html__( 'flipInX', 'xg-product' ),
					'flipInY'            => esc_html__( 'flipInY', 'xg-product' ),
					'flipOutX'           => esc_html__( 'flipOutX', 'xg-product' ),
					'flipOutY'           => esc_html__( 'flipOutY', 'xg-product' ),
					'lightSpeedIn'       => esc_html__( 'lightSpeedIn', 'xg-product' ),
					'lightSpeedOut'      => esc_html__( 'lightSpeedOut', 'xg-product' ),
					'rotateIn'           => esc_html__( 'rotateIn', 'xg-product' ),
					'rotateInDownLeft'   => esc_html__( 'rotateInDownLeft', 'xg-product' ),
					'rotateInDownRight'  => esc_html__( 'rotateInDownRight', 'xg-product' ),
					'rotateInUpLeft'     => esc_html__( 'rotateInUpLeft', 'xg-product' ),
					'rotateInUpRight'    => esc_html__( 'rotateInUpRight', 'xg-product' ),
					'rotateOut'          => esc_html__( 'rotateOut', 'xg-product' ),
					'rotateOutDownLeft'  => esc_html__( 'rotateOutDownLeft', 'xg-product' ),
					'rotateOutDownRight' => esc_html__( 'rotateOutDownRight', 'xg-product' ),
					'rotateOutUpLeft'    => esc_html__( 'rotateOutUpLeft', 'xg-product' ),
					'rotateOutUpRight'   => esc_html__( 'rotateOutUpRight', 'xg-product' ),
					'slideInUp'          => esc_html__( 'slideInUp', 'xg-product' ),
					'slideInDown'        => esc_html__( 'slideInDown', 'xg-product' ),
					'slideInLeft'        => esc_html__( 'slideInLeft', 'xg-product' ),
					'slideInRight'       => esc_html__( 'slideInRight', 'xg-product' ),
					'slideOutUp'         => esc_html__( 'slideOutUp', 'xg-product' ),
					'slideOutDown'       => esc_html__( 'slideOutDown', 'xg-product' ),
					'slideOutLeft'       => esc_html__( 'slideOutLeft', 'xg-product' ),
					'slideOutRight'      => esc_html__( 'slideOutRight', 'xg-product' ),
					'zoomIn'             => esc_html__( 'zoomIn', 'xg-product' ),
					'zoomInDown'         => esc_html__( 'zoomInDown', 'xg-product' ),
					'zoomInLeft'         => esc_html__( 'zoomInLeft', 'xg-product' ),
					'zoomInRight'        => esc_html__( 'zoomInRight', 'xg-product' ),
					'zoomInUp'           => esc_html__( 'zoomInUp', 'xg-product' ),
					'zoomOut'            => esc_html__( 'zoomOut', 'xg-product' ),
					'zoomOutDown'        => esc_html__( 'zoomOutDown', 'xg-product' ),
					'zoomOutLeft'        => esc_html__( 'zoomOutLeft', 'xg-product' ),
					'zoomOutRight'       => esc_html__( 'zoomOutRight', 'xg-product' ),
					'zoomOutUp'          => esc_html__( 'zoomOutUp', 'xg-product' ),
					'hinge'              => esc_html__( 'hinge', 'xg-product' ),
					'jackInTheBox'       => esc_html__( 'jackInTheBox', 'xg-product' ),
					'rollIn'             => esc_html__( 'rollIn', 'xg-product' ),
					'rollOut'            => esc_html__( 'rollOut', 'xg-product' ),
				],
				'default' => 'fadeOut',
				'description' => esc_html__( 'you can set animation in for slider', 'appside-master' ),
			]
		);
		$this->end_controls_section();
		$this->start_controls_section(
			'css_styles',
			[
				'label' => esc_html__( 'Padding', 'attorg-master' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control( 'padding', [
			'label'              => esc_html__( 'Padding', 'attorg-master' ),
			'type'               => Controls_Manager::DIMENSIONS,
			'size_units'         => [ 'px', 'em' ],
			'allowed_dimensions' => [ 'top', 'bottom' ],
			'selectors'          => [
				'{{WRAPPER}} .header-area' => 'padding-top: {{TOP}}{{UNIT}};padding-bottom: {{BOTTOM}}{{UNIT}};'
			],
			'description'        => esc_html__( 'set padding for header area ', 'attorg-master' )
		] );
		$this->end_controls_section();
	}

	/**
	 * Render Elementor widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		$settings         = $this->get_settings_for_display();
		$all_header_items = $settings['header_sliders'];
		$rand_numb        = rand( 333, 999999999 );

		//slider settings
		$loop            = $settings['loop'] ? 'true' : 'false';
		$nav             = $settings['nav'] ? 'true' : 'false';
		$items           = $settings['items'] ? $settings['items'] : 4;
		$autoplay        = $settings['autoplay'] ? 'true' : 'false';
		$autoplaytimeout = $settings['autoplaytimeout']['size'];
		$row_class = '';
		
		if($settings['slider_alignment'] == 'right'){
			$row_class = 'justify-content-end';
		}elseif($settings['slider_alignment'] == 'center'){
			$row_class = 'justify-content-center';
		}
		?>
        <header class="header-carousel-wrapper attorg-rtl-slider">
            <div class="header-carousel-one owl-carousel header-slider-one wol-header-one"
            id="header-one-carousel-<?php echo esc_attr( $rand_numb ); ?>"
                 data-loop="<?php echo esc_attr( $loop ); ?>"
                 data-nav="<?php echo esc_attr( $nav ); ?>"
                 data-margin="<?php echo esc_attr( $settings['margin']['size'] ); ?>"
                 data-items="<?php echo esc_attr( $items ); ?>"
                 data-autoplay="<?php echo esc_attr( $autoplay ); ?>"
                 data-autoplaytimeout="<?php echo esc_attr( $autoplaytimeout ); ?>"
                 data-animatein="<?php echo esc_attr( $settings['animate_in'] ); ?>"
                 data-animateout="<?php echo esc_attr( $settings['animate_out'] ); ?>"
            >
				<?php
				foreach ( $all_header_items as $item ):
					$img_url_desktop = wp_get_attachment_image_src($item['background_image_desktop']['id'],'full');
					$img_url_mobile = wp_get_attachment_image_src($item['background_image_mobile']['id'],'full');
					$btn_margin_color = $item['description_status'] != 'yes' ? 'no-description' : '';

					?>
                    <div class="header-area header-bg <?php echo esc_attr($btn_margin_color);?> align-<?php echo esc_attr($settings['slider_alignment']);?>">
                        <img data-object-fit='cover' class="header-background --desktop" src="<?php echo esc_url($img_url_desktop[0])?>" alt="background desk">
                        <img data-object-fit='cover' class="header-background --mobile d-lg-none" src="<?php echo esc_url($img_url_mobile[0])?>" alt="background mobile">
                        <div class="container-fluid">
                            <div class="row <?php echo esc_attr($row_class);?> align-items-end">
                                <div class="col-sm-10 offset-md-1">
                                    <div class="header-inner">
										<?php if ( 'yes' == $item['subtitle_status'] ): ?>
                                            <span class="subtitle"><?php echo esc_html( $item['subtitle'] ) ?></span>
										<?php endif ?>
										<h2 class="title"><?php echo esc_html( $item['title'] ) ?></h2>
										<?php if($item['description_status'] == 'yes'):?>
										<p><?php echo esc_html( $item['description'] ) ?></p>
										<?php endif;?>
                                        <div class="btn-wrapper  desktop-left padding-top-30">
											<?php if ( 'yes' == $item['btn_one_status'] ): ?>
                                                <a
                                                        href="<?php echo esc_url( $item['btn_one_url']['url'] ); ?>"
                                                        class="boxed-btn blank"
													<?php echo ( $item['btn_one_url']['is_external'] ) ? 'target="_blank"' : ''; ?>
                                                ><?php echo esc_html( $item['btn_one_text'] ) ?></a>
											<?php endif; ?>
											<?php if ( 'yes' == $item['btn_two_status'] ): ?>
                                                <a
                                                        href="<?php echo esc_url( $item['btn_two_url']['url'] ); ?>"
													<?php echo ( $item['btn_two_url']['is_external'] ) ? 'target="_blank"' : ''; ?>
                                                        class="boxed-btn "><?php echo esc_html( $item['btn_two_text'] ) ?></a>
											<?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				<?php endforeach; ?>
            </div>
        </header>
		<?php
	}
}

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Attorg_Wol_Header_Area_One_Widget() );