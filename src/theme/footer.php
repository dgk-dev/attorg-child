<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package attorg
 */
$copyright_text = !empty(cs_get_option('copyright_text')) ? cs_get_option('copyright_text'): esc_html__('Attorg Theme Developed by Ir-Tech','attorg');
$copyright_text = str_replace('{copy}','&copy;',$copyright_text);
$copyright_text = str_replace('{year}',date('Y'),$copyright_text);
?>

	</div><!-- #content -->
<footer class="footer-area">
    <?php get_template_part('template-parts/common/footer-widget'); ?>

    <div class="copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="copyright-inner text-left">
	                    <?php
	                    echo wp_kses($copyright_text,Attorg()->kses_allowed_html(array('a')));
	                    ?>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 text-right">
                    Hecho por <a href="https://dgk.com.mx/" target="_BLANK">dgk</a>
                </div>
            </div>
        </div>
    </div>
</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
