<?php
/**
 * Author Ir-Tech
 * @since 1.0.0
 * */

if (!defined('ABSPATH')){
	exit(); //exit if access directly
}

// Wordpress cleanup
require(get_stylesheet_directory().'/includes/cleanup.php');

// Resources
require(get_stylesheet_directory().'/includes/resources.php');

// Widgets
require(get_stylesheet_directory().'/includes/widgets.php');

// Elementor
require(get_stylesheet_directory().'/elementor/class-elementor-widget-init.php');