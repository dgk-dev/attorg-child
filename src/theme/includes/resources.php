<?php
function wol_enqueue_styles() {
	wp_enqueue_style( 'attorg-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		array('main-style', 'attorg-main-style'),
		'1.0.0'
	);

	wp_register_script( 'wol-functions', get_stylesheet_directory_uri() . '/js/footer-bundle.js', null, '1.0', true);
	// wp_localize_script('wol-functions', 'WolGlobalObject', array(
	// 	'home_url' => home_url(),
	// ));
	wp_enqueue_script( 'wol-functions' );
}

add_action( 'wp_enqueue_scripts', 'wol_enqueue_styles', 11 );

add_action('wp_footer', 'wol_data');
function wol_data(){
?>
	<script type=application/ld+json>
		{
			"@context": "http://schema.org",
			"@type": "Organization",
			"name": "WOL",
			"url": "<?php echo home_url(); ?>",
			"address": "Gobernación #12 Col. 5 de diciembre. Morelia, Mich.",
			"image": "<?php echo get_stylesheet_directory_uri(); ?>/img/wol-logo-social.png",
			"description": "Espacios virtuales para gestionar tu negocio desde cualquier lugar. Profesionaliza tu imagen: No des la dirección de tu casa o la del café donde sueles trabajar. Con WOL, Oficinas virtuales, nosotros recibiremos toda tu mensajeria y te ayudaremos a gestionarla. Tus clientes atendidos. Con WOL, Oficinas virtuales, contarás con servicio de contestación de llamadas personalizadas.",
			"sameAs": [
				"https://www.facebook.com/WOLCenter",
				"https://www.instagram.com/wolcentermorelia",
				"https://www.linkedin.com/company/wol-center"
			]
		}
	</script>
<?php
}

add_action('wp_footer', 'wol_analytics');
function wol_analytics(){
	?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-163835004-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
          dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-163835004-1');
      </script>

	<script>
		document.addEventListener( 'wpcf7mailsent', function( event ) {
			gtag('event', 'Lead', { 'event_category' : 'Convertion' });
			var inputs = event.detail.inputs;
			for (var i=0; i < inputs.length; i++){
				if (inputs[i]['name'] == 'servicio'){
					gtag('event', 'Lead', { 'event_category' : 'Conversiones', 'event_label': inputs[i]['value'] });
					return;
				}
			}
		}, false );
	</script>
	<?php
}

add_action('wp_head', 'wol_fb_pixel');
function wol_fb_pixel(){
	?>
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '732999610500521');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=732999610500521&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	<?php
}