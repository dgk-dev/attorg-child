<?php
function wol_widget_rewrite($params)
{
    foreach($params as $key => $param){
        if($param['id'] == 'footer-widget'){
            $params[$key]['before_widget'] = '<div class="col-lg-12 col-md-12"><div id="attorg_about_us-1" class="widget footer-widget widget_attorg_about_us text-center">';
        }
    }
    return $params;
}
add_filter('dynamic_sidebar_params', 'wol_widget_rewrite', 10);